<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>

<jsp:useBean id="commentMgrObj" class="comment.CommentManager" />
<jsp:useBean id="myDB" class="member.StudentManager" />

<% 
session = request.getSession(true);
String userid=(String)session.getValue("id");

if (userid == null) {
%>
	<script>
   		window.alert('You are not logged in!!');
   		history.go(-1);
	</script>
<%	
}
else {
	String  comment		= request.getParameter("comment");
	String 	writerId	= request.getParameter("writerId");
	int 	issueId		= Integer.parseInt(request.getParameter("issueId"));
	// 입력값 null 여부는 .jsp쪽에서 javascript로 해준다고 가정... 입력값은 모두 valid하다!

	try {
		commentMgrObj.addComment(comment, writerId, issueId);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	
	response.sendRedirect("./4_issue_page.jsp?id="+ issueId);
}
%>

<!-- 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<body>

</body>
</html>
 -->