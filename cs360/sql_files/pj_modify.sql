/* project modification*/

/*1. Retrieve current information */
/*1-1. project info w/o participants' name*/


SELECT p.id, p.title, p.proj_desc, s.name as leadername, TO_CHAR(p.start_date,'YYYY-MM-DD') as startdate,TO_CHAR(p.due_date,'YYYY-MM-DD') as duedate,p.course_cd
FROM PROJECT as p join STUDENT as s on p.leader_id = s.id
where p.id = 0;


/* p.id =0 : 0 to variable */

/*1-2. project ino - participant's name*/


SELECT s.name, s.id
FROM PROJECT_PARTICIPANT as pp join STUDENT as s on pp.participant_id = s.id
where pp.project_id = 0;


/* pp.project_id = 0 : 0 to variable */


/*2. update new information */

/*2-1. update project info*/


update project
set proj_desc = 'Design web service with Oracle!!', start_date= TO_DATE('06/06/2013', 'MM/DD/YYYY'), due_date = TO_DATE('07/13/2013', 'MM/DD/YYYY'),course_cd = 'CS101'
where id=0;


/* 'Design~' : input(desc)
    '06/06/2013~' : input(start date)
  '07/13/2013~' : input(due date)
	'cs101' : input(course cd)
	id=0 : 0 -> variable(project id) */




/*2-2.delete participant info*/

delete from project_participant
where project_id = 0 and participant_id = 'magoon';
/* 0 : variable (project id)
	'magoon' : variable(student id)*/
/*2-3.insert participant info*/ 

INSERT INTO PROJECT_PARTICIPANT 
VALUES (0, 'magoon');
/* 0 : variable (project id)
	'magoon' : variable(student id)*/
	
	

/*3. Trigger : trig.sql */
