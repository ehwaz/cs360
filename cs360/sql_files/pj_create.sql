/*create new project*/

INSERT INTO PROJECT
VALUES((select count(rownum) from PROJECT), 
'sql tuning','tune your db','sweetko',
TO_DATE(TO_CHAR(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'),
TO_DATE('06/22/2013','MM/DD/YYYY'),TO_DATE('06/25/2013','MM/DD/YYYY'),TO_DATE(TO_CHAR(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'),'CS360','magoon');

/* 'sql tuning' ->variable(title)
  'tune your db' -> variable(description)
	'sweetko' -> variable(current user)(creator id)
	'06/22/2013' -> variable(start date)
	'06/25/2013' -> variable(due date)
	'CS360' ->variable(course_cd)
	'magoon'->variable(leader_id)
	last_modified_date-> set as current sysdate. */
