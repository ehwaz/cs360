/* project information */

/*1. project info w/o participants' name*/
SELECT p.id, p.title, p.proj_desc, s.name, TO_CHAR(p.start_date,'YYYY-MM-DD') as startdate,TO_CHAR(p.due_date,'YYYY-MM-DD') as duedate,p.course_cd
FROM PROJECT as p join STUDENT as s on p.leader_id = s.id
where p.id = 0;

/* p.id =0 : 0 to variable */

/*2. project ino - participant's name*/
SELECT s.name
FROM PROJECT_PARTICIPANT as pp join STUDENT as s on pp.participant_id = s.id
where pp.project_id = 0;
/* pp.project_id = 0 : 0 to variable */

/*3. issue list*/

SELECT ROWNUM, i.title, TO_CHAR(i.create_date,'YYYY-MM-DD') as created, TO_CHAR(i.deadline_date, 'YYYY-MM-DD') as deadline, i.done, s.name as incharge
FROM issue as i join student as s on i.incharge_id = s.id
where i.project_id = 0;

/* i.project_Id = 0 : 0 to variable*/
