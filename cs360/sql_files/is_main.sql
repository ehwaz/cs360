/* issue main page*/

/* 1. retrieve issue info*/

select i.title, i.contents, TO_CHAR(i.create_date,'YYYY-MM-DD') as createdate, TO_CHAR(i.deadline_date,'YYYY-MM-DD') as deadlinedate, i.done, s.name
from issue as i join student as s on i.incharge_id = s.id
where i.id = 0;
/* id=0 : 0 -> variable*/


/* 2. retrieve task info */

select ROWNUM, contents, done
from task
where issue_id = 0;
/* issue_id = 0 : 0->variable */

/* 3. retrieve comments */
select c.contents, s.name, TO_CHAR(c.write_date,'YYYY-MM-DD') as writedate
from comments as c join student as s on c.writer_id = s.id
where c.issue_id = 0;
/* issue_id = 0 : 0->variable*/

/* 4. done/undone issue */
/*4-1. done*/
update issue
set done = bit'1'
where id = 0;
/* id=0 : 0 ->variable */

/*4-2. undone*/
update issue
set done = bit'0'
where id = 0;
/* id=0 : 0->variable */

/* 5. add task */

insert into task
values ((select count(rownum) from task),'learn spring framework',bit'0',1);
/* 'learn spring framework' : input(contents)*/
/* 1 : 1->variable(issue id)*/

/* 6. done/undone task */

/* 6-1. done*/
update task
set done = bit'1'
where issue_id = 1;
/* issue_id = 1 : 1 -> variable */

/* 6-2. undone*/
update task
set done = bit'0'
where issue_id = 0;
/* issue_id = 0 : 0 -> variable*/

/*7. add comments */
insert into comments
values((select count(rownum) from comments),'hurry up!',TO_DATE(TO_CHAR(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'),'magoon',1);

/* hurry up! : input(contents)*/
/* 'magoon' : input(writer_id)*/
/* 1 : input(issue_id)*/
