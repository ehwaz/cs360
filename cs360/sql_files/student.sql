/* create student table*/
CREATE TABLE STUDENT(
  id varchar(15) PRIMARY KEY,
	password varchar(40) not null,
	name varchar(30) not null,
	email varchar(30) not null,
	phonenumber integer not null,
	image BLOB
);

/* (2)assign new student*/
INSERT INTO STUDENT 
VALUES('sweetko', desencrypt('12345678', 'password'),'Hyunseok Lee','sweetko08@gmail.com',01062939561,NULL);

/*(10)get student info*/
SELECT id,name,email,phonenumber,BINARY_LENGTH(image)
FROM STUDENT
WHERE id='sweetko';
