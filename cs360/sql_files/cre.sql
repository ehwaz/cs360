CREATE TABLE COURSE(
  course_cd varchar(10) PRIMARY KEY, 
	course_name varchar(50) not null,
	prof_name varchar(50) not null
);

CREATE TABLE AUTH(
	a_type integer PRIMARY KEY,
	project bit(1) not null,
	issue bit(1) not null
);

CREATE TABLE MESSAGE(
	m_id integer PRIMARY KEY,
	source varchar(15) not null,
	s_id integer not null,
	m_desc varchar(5000) not null
); /* 안쓸 계획이다. */

create message_proj(
	 m_id integer primary key,
	 proj_id integer not null,
	 m_desc varchar(5000) not null,
	 create_date date not null
); /* message_issue 구현 후 구현할 계획이다. */
 
create table message_issue(
	 m_id integer primary key,
	 issue_id integer not null,
	 m_desc varchar(5000) not null,
	 create_date date not null
);
 
CREATE TABLE STUDENT(
	id varchar(15) PRIMARY KEY,
	password varchar(15) not null,
	name varchar(50) not null,
	email varchar(50) not null,
	phonenumber integer not null,
	image varchar(100),
	auth_type integer not null,
	constraint fk_student_auth_type foreign key (auth_type) references AUTH(a_type)
);



CREATE TABLE PROJECT(
	id integer PRIMARY KEY,
	title varchar(30) not null,
	proj_desc  varchar(5000) not null,
	creator varchar(15) not null,
	create_date date not null,
	start_date date not null,
	due_date date not null,
	last_modified_date date not null,
	course_cd varchar(10) not null,
	leader_id varchar(15) not null,
	constraint fk_project_course_cd foreign key (course_cd) references COURSE (course_cd),
	constraint fk_project_leader_id foreign key (leader_id) references STUDENT (id)
	
);

CREATE TABLE ISSUE(
	id integer PRIMARY KEY,
	title varchar(30) not null,
	contents varchar(5000) not null,
	create_date date not null,
	deadline_date date not null,
	last_modified_date date not null,
	done integer not null,
	writer_id varchar(15) not null,
	incharge_id varchar(15),
	project_id integer not null,
	constraint fk_issue_writer_id foreign key (writer_id) references STUDENT(id),
	constraint fk_issue_incharge_id foreign key (incharge_id) references STUDENT (id),
	constraint fk_issue_project_id foreign key (project_id) references PROJECT (id)
);

CREATE TABLE PROJECT_PARTICIPANT(
	project_id integer not null,
	participant_id varchar(15) not null,
	constraint fk_proj_partic_project_id foreign key (project_id) references PROJECT(id),
	constraint fk_proj_partic_participant_id foreign key (participant_id) references STUDENT(id),
	primary key(project_id, participant_id)
);

CREATE TABLE TASK(
	id integer PRIMARY KEY,
	contents varchar(5000) not null,
	done integer not null,
	issue_id integer not null,
	constraint fk_task_issue_id foreign key (issue_id) references ISSUE(id)
);

CREATE TABLE COMMENTS(
	id integer PRIMARY KEY,
	contents varchar(5000) not null,
	write_date date not null,
	writer_id varchar(15) not null,
	issue_id integer not null,
	constraint fk_comm_writer_id foreign key (writer_id) references STUDENT(id),
	constraint fk_comm_issue_id foreign key (issue_id) references ISSUE(id)
);
CREATE TABLE usr_list(
	m_id integer not null,
	usr_id varchar(15) not null,
	PRIMARY KEY(m_id, usr_id),
	constraint fk_usr_list_m_id foreign key (m_id) references MESSAGE(m_id),
	constraint fk_usr_list_usr_id foreign key(usr_id) references STUDENT(id)
);/* 안쓸 예정이다. */

CREATE TABLE usr_issue_list(
	m_id integer not null,
	usr_id varchar(15) not null,
	PRIMARY KEY(m_id, usr_id),
	constraint fk_usr_issue_list_m_id foreign key (m_id) references MESSAGE_issue(m_id),
	constraint fk_usr_issue_list_usr_id foreign key(usr_id) references STUDENT(id)
);

CREATE TABLE usr_proj_list(
	m_id integer not null,
	usr_id varchar(15) not null,
	PRIMARY KEY(m_id, usr_id),
	constraint fk_usr_proj_list_m_id foreign key (m_id) references MESSAGE_proj(m_id),
	constraint fk_usr_proj_list_usr_id foreign key(usr_id) references STUDENT(id)
);
