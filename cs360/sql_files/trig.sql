/* trigger creation*/

/*1. project modification */

/*1-1. trigger on project*/


CREATE TRIGGER proj_trig
AFTER UPDATE OF proj_desc,start_date,due_date,course_cd ON PROJECT
REFERENCING NEW ROW NEW_ROW 
FOR EACH ROW
AS
m_id integer;
BEGIN
/*  insert into message	values ((select count(rownum) from message),'PROJECT',NEW_ROW.id,'proj_desc/'||NEW_ROW.proj_desc||'/start_date/'||TO_CHAR(NEW_ROW.start_date,'YYYY-MM-DD')||'/due_date/'||TO_CHAR(NEW_ROW.due_date,'YYYY-MM-DD')||'/course_cd/'||NEW_ROW.course_cd);*/
	select count(rownum) into m_id from message_proj;
	insert into message_proj
	values (m_id, new_row.id, 'Project "'||new_row.title||'" is updated!!', to_date(to_char(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'));
	INSERT INTO usr_proj_list 
	values (m_id, new_row.leader_id);
END;
/


/*1-2. trigger on project_participant add*/

CREATE TRIGGER proj_part_trig
AFTER INSERT ON PROJECT_PARTICIPANT
REFERENCING NEW ROW NEW_ROW 
FOR EACH ROW
AS
m_id integer;
BEGIN
	/*insert into message	values ((select count(rownum) from message),'PROJECT',NEW_ROW.project_id,'project_id/'||NEW_ROW.project_id||'/participant_id/'||NEW_ROW.participant_id);*/
	select count(rownum) into m_id from message_proj;
	insert into message_proj
	values (m_id, new_row.project_id, 'You are now in Project #'||new_row.project_id||'!!', to_date(to_char(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'));
	INSERT INTO usr_proj_list 
	values (m_id, new_row.participant_id);
END;
/


/*2. issue modification */

/*2-1. trigger on issue insert*/

/*
CREATE TRIGGER issue_insert_trig
AFTER INSERT ON ISSUE
REFERENCING NEW ROW NEW_ROW 
FOR EACH ROW
AS BEGIN 
	/*insert into message values ((select count(rownum) from message), 'ISSUE',NEW_ROW.id,'title/'||NEW_ROW.title||'/contents/'||NEW_ROW.contents||'/deadline_date/'||NEW_ROW.deadline_date||'/incharge_id/'||NEW_ROW.incharge_id||'/done/'||NEW_ROW.done);*/
	insert into message_issue 
	values ((select count(rownum) from message_issue), NEW_ROW.id, 'New issue "'||new_row.title||'" is added!!', to_date(to_char(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'));
END;
/
 */

/*2-1. trigger on issue update(done, info)*/

/*
CREATE TRIGGER issue_update_trig
AFTER UPDATE OF title,contents,deadline_date,incharge_id,done ON ISSUE
REFERENCING NEW ROW NEW_ROW 
FOR EACH ROW
AS BEGIN 
	insert into message values ((select count(rownum) from message), 'ISSUE',NEW_ROW.id,'title/'||NEW_ROW.title||'/contents/'||NEW_ROW.contents||'/deadline_date/'||NEW_ROW.deadline_date||'/incharge_id/'||NEW_ROW.incharge_id||'/done/'||NEW_ROW.done);
END;
/
*/

/*2-2. trigger on task add,update(contents, done)*/

/*
CREATE TRIGGER task_insert_trig
AFTER INSERT ON TASK
REFERENCING NEW ROW NEW_ROW
FOR EACH ROW
AS BEGIN
	/* insert into message values ((select count(rownum) from message),'ISSUE',NEW_ROW.issue_id,'task/'||'contents/'||NEW_ROW.contents||'/done/'||NEW_ROW.done);*/
	insert into message_issue 
	values ((select count(rownum) from message_issue),NEW_ROW.issue_id,'New task "'||new_row.contents||'" is added!!', to_date(to_char(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'));
END;
/
*/

/*
/* content of task cannot be changed. */
CREATE TRIGGER task_update_trig
AFTER UPDATE OF done ON TASK
REFERENCING NEW ROW NEW_ROW
FOR EACH ROW
AS BEGIN
	insert into message values ((select count(rownum) from message),'ISSUE',NEW_ROW.issue_id,'task/'||'contents/'||NEW_ROW.contents||'/done/'||NEW_ROW.done);

END;
/
*/

/*2-3. trigger on comments add*/

/*
CREATE TRIGGER comment_insert_trig
AFTER INSERT ON COMMENTS
REFERENCING NEW ROW NEW_ROW
FOR EACH ROW
AS BEGIN
	/*insert into message values((select count(rownum) from message),'ISSUE',NEW_ROW.issue_id,'comments/'||'contents/'||NEW_ROW.contents||'/write_date/'||NEW_ROW.write_date||'/writer_id/'||NEW_ROW.writer_id);*/
	insert into message_issue 
	values((select count(rownum) from message_issue), NEW_ROW.issue_id, 'New comment "'||new_row.contents||'" is added!!', TO_DATE(TO_CHAR(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'));
END;
/
*/

/*3. trigger inserting rows in usr_list table*/
/*
CREATE TRIGGER usr_issue_trig
AFTER INSERT ON MESSAGE_ISSUE
REFERENCING NEW ROW NEW_ROW
FOR EACH ROW
AS 
p_id integer;
BEGIN
SELECT i.project_id into p_id from issue as i where i.id=NEW_ROW.issue_id;

DECLARE
CURSOR c1 IS 
SELECT p.participant_id from project_participant as p where p.project_id=p_id;

BEGIN
FOR usr_id IN c1 LOOP
	INSERT INTO usr_issue_list values(NEW_ROW.m_id , usr_id.participant_id);
END LOOP;
END;

END;
/

CREATE TRIGGER usr_proj_trig
AFTER INSERT ON MESSAGE_PROJ
REFERENCING NEW ROW NEW_ROW
FOR EACH ROW
AS 
p_id integer;
BEGIN
p_id := NEW_ROW.proj_id;

DECLARE
CURSOR csr1 IS 
SELECT participant_id from project_participant where project_id=p_id;

BEGIN
FOR usr_id IN csr1 LOOP
	INSERT INTO usr_proj_list values(NEW_ROW.m_id , usr_id.participant_id);
END LOOP;
END;

END;
/
*/
/*
CREATE TRIGGER usr_trig
AFTER INSERT ON MESSAGE
REFERENCING NEW ROW NEW_ROW
FOR EACH ROW
AS 
s_type varchar(15);
p_id integer;
BEGIN
s_type := NEW_ROW.source;

IF s_type = 'PROJECT' then
	p_id := NEW_ROW.s_id;
ELSIF s_type = 'ISSUE' then
BEGIN
	SELECT project_id into p_id from issue where id=NEW_ROW.s_id;
END;
END IF;

DECLARE
CURSOR c1 IS 
SELECT p.participant_id from project_participant as p where p.project_id=p_id;

BEGIN
FOR usr_id IN c1 LOOP
	INSERT INTO usr_list values(NEW_ROW.m_id , usr_id.participant_id);
END LOOP;
END;
	

END;
/
*/
