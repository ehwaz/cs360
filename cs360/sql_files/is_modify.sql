/* issue modification */


/*1. Retrieve current information */
select title,contents, TO_CHAR(deadline_date,'YYYY-MM-DD') as deadlinedate, writer_id, incharge_id, project_id, done
from issue
where id=0;
/* id=0 : 0 -> variable*/

select ROWNUM, contents, done
from task
where issue_id = 0;
/* issue_id = 0 : 0->variable */


/*2. Update */
/*2-1. update issue table*/


update issue
set title='insert image', contents='using link or blob', deadline_date=TO_DATE('06/24/2013','MM/DD/YYYY'),incharge_id='magoon'
where id=0;

/*'insert image' : input
  'using link or blob' : input
  '06/24/2013' : input
  'magoon' : input
  id=0 : 0 : input */

/*2-2. update task table*/

update task
set contents = 'change color'
where issue_id = 2;

/* 'change color' : input
  2 : input*/
