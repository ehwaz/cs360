<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="member.*, java.util.*, java.text.*, java.io.*" %>
 <jsp:useBean id="myDB" class="member.StudentManager" scope="page"/>
 
  <%
  session=request.getSession(true);
  String idFromSession = (String)session.getValue("id");
  
  if (idFromSession == null) {
	  response.sendRedirect("./11_login_page.jsp");
  }
  
   //deleteMyMem()메서드를 이용해서 해당회원을 삭제한다.
   myDB.deleteStudent(idFromSession);

   // 프로필 파일을 삭제한다.
   String filePath = "C:/Users/MaGoon/Desktop/user_photo/" + idFromSession + ".jpg";
   File profile = new File(filePath);
   profile.delete();
   
   //여기서도 똑같이 회원이 삭제되었으니까..
   //록인창에 세션값을 가질 필요가 없으니까..
   //해당되는 "id"값의 세션을 지우는것이 맞겠죠^^

   session.removeValue("id");
   response.sendRedirect("./11_login_page.jsp");
  %>
  
 <!--
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<body>

</body>
</html>
  -->