 $(document).ready(function(){
	 var percent = parseInt($('#percent').val());
	$("#mario").animate({
		left:'+=' + 336*percent/100,},
		{duration: 2000,
		 step: function(now,fx){
			$('#percent_mario').text( Math.floor( fx.pos * percent) + '%' ); 
		}
	});

	$("input[type=checkbox].switch").each(function() {
		// Insert mark-up for switch
		$(this).before(
		  '<span class="switch">' +
		  '<span class="mask" /><span class="background" />' +
		  '</span>'
		);
		$(this).hide();
		if (!$(this)[0].checked) {
		  $(this).prev().find(".background").css({left: "-56px"});
		}
	 });

	 $("span.switch").click(function() {
		// If on, slide switch off
		if ($(this).next()[0].checked) {
		  $(this).find(".background").animate({left: "-56px"}, 200);
		// Otherwise, slide switch on
		} else {
		  $(this).find(".background").animate({left: "0px"}, 200);
		}
		// Toggle state of checkbox
		$(this).next()[0].checked = !$(this).next()[0].checked;
  });

});