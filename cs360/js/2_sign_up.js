
function validateForm(){
	if($("#id").val() == "") {
		alert("Please write ID!");
		$("#id").focus();
		return false;
	}
	if($("#password").val() == "") {
		alert("Please write password!");
		$("#password").focus();
		return false;
	}
	if($("#name").val() == "") {
		alert("Please write name!");
		$("#name").focus();
		return false;
	}
	if($("#email").val() == "") {
		alert("Please write a person in email!");
		$("#email").focus();
		return false;
	}
	if($("#phone").val() == "") {
		alert("Please write phone number!");
		$("#phone").focus();
		return false;
	}

	return true;
}
