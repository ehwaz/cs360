 $(document).ready(function(){
	$( ".datepick" ).datepicker({ dateFormat: 'yy-mm-dd' });
});

function validateForm2(){
	if($("#title").val() == "") {
		alert("Please write title!");
		$("#title").focus();
		return false;
	}
	if($("#description").val() == "") {
		alert("Please write description!");
		$("#description").focus();
		return false;
	}
	if($("#due_date").val() == "") {
		alert("Please write due date!");
		$("#due_date").focus();
		return false;
	}
	if($("#start_date").val() == "") {
		alert("Please write start date!");
		$("#start_date").focus();
		return false;
	}
	if($("#course").val() == "") {
		alert("Please write course!");
		$("#course").focus();
		return false;
	}
	if($("#leader").val() == "") {
		alert("Please write a leader!");
		$("#leader").focus();
		return false;
	}
	

	return true;
}
