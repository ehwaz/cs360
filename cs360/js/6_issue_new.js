 $(document).ready(function(){
	$( ".datepick" ).datepicker({ dateFormat: 'yy-mm-dd' });
	
	$("#add_task").click( function(){
		if($("#task_name").val()==""){
			alert("You should write task title.");
			return false;
		}
		$('#task').append("<option>" + $("#task_name").val() + "</option>");
		$("#task_name").val("");
	});
});




function validateForm(){
	if($("#title").val() == "") {
		alert("Please write title!");
		$("#title").focus();
		return false;
	}
	if($("#description").val() == "") {
		alert("Please write description!");
		$("#description").focus();
		return false;
	}
	if($("#deadline").val() == "") {
		alert("Please write deadline!");
		$("#deadline").focus();
		return false;
	}
	if($("#charge").val() == "") {
		alert("Please write a person in charge!");
		$("#charge").focus();
		return false;
	}


	return true;
}
