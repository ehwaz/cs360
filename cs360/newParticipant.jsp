<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>

<jsp:useBean id="myMem" class="member.StudentBean" />
<jsp:useBean id="myDB" class="member.StudentManager" />

<jsp:useBean id="projObj" class="project.ProjectBean" />
<jsp:useBean id="projMgrObj" class="project.ProjectManager" />

<jsp:useBean id="partBean" class="participant.ParticipantBean" />
<jsp:useBean id="partManager" class="participant.ParticipantManager" />

<% 
session = request.getSession(true);
String userid=(String)session.getValue("id");

if (userid == null) {
%>
	<script>
   		window.alert('You are not logged in!!');
   		history.go(-1);
	</script>
	
<%	
}
else {
	String student_id		= request.getParameter("addid");
	String proj_id			= request.getParameter("pid");
	
	if (myDB.checkStudent(student_id)){
		
		if(partManager.alreadyParticipate(proj_id, student_id)){
%>
	<script>
		window.alert('ID already Participating!!');
		history.go(-1);
	</script>
<%
			
		}
		else{
			try {
				partManager.registerParticipant(student_id, proj_id);} 
			catch (Exception e) {
			    e.printStackTrace();}
			response.sendRedirect("./3_project_page.jsp?id=" + proj_id);
		}
	}
	else{
%>
	<script>
		window.alert('Student ID not found!!');
		history.go(-1);
	</script>
<% 	
	}	
}
%>