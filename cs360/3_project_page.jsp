<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ page import="java.util.ArrayList" %>	
<%@ page import="project.ProjectBean" %>
<%@ page import="issue.IssueBean" %>
<%@ page import="participant.ParticipantBean" %>
<%@ page import="member.StudentBean" %>

 <jsp:useBean id="myMem" class="member.StudentBean" />
 <jsp:useBean id="myDB" class="member.StudentManager" />
 
<jsp:useBean id="projMgrObj" class="project.ProjectManager" />
<jsp:useBean id="issuemng" class="issue.IssueManager" />
<jsp:useBean id="partiManager" class="participant.ParticipantManager" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<title> Oui Cole </title>
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
		<script type="text/JavaScript" src="js/3_project_page.js"></script>
	</head>

	<body>
		<div class="top">
			<p><a href="1_project_list.jsp"><img src="images/logo.PNG" alt="Oui Cole" width="160" height="50"></a></p>
		</div>
		<hr>
		
		<% 
			String id = request.getParameter( "id" ); 	// 이게 프로젝트 아이디!!
			
			session=request.getSession(true);
			String userid=(String)session.getValue("id"); // User ID
			ArrayList<IssueBean> returnList = null; // 사용자 아이디에 대한 
			ArrayList<StudentBean> returnList1 = null;
			
		   	if(userid != null ) {
		   		myMem = myDB.getStudent(userid);
		   		ProjectBean project = projMgrObj.getProjectByProjectId(id);
		   		returnList = issuemng.getIssueByProj(id);
		   		returnList1 = partiManager.getUserByProj(id);
		   		double percent = -1;
		   		if(issuemng.issueTotal(id) == 0)
		   		{percent = 0;}
		   		else
		   		{percent = (double)issuemng.issueDone(id)/(double)issuemng.issueTotal(id);}
		%>
		<input type="hidden" id="percent" value=<%out.print(100*percent);%>>
		<div class="wrapper">
			<div class="left_navi">
				<table class="member_info">
					<colgroup>
						<col width="50%">
						<col width="50%">
					</colgroup>
					<tr align="center">
						<td colspan="2" align="center"><img src="user_photo/
					<%
					out.print(myMem.getId());
					%>
					.jpg" alt="Profile" width="120" height="150" class="profile"></td>
					</tr>
					<tr align="center">
						<td colspan="2"> <% out.print(myMem.getName()); %>
 [<% out.print(myMem.getId()); %>]</td>
					</tr>
					<tr align="center">
						<td colspan="2"><% out.print(myMem.getEmail()); %></td>
					</tr>
					<tr align="center">
						<td colspan="2"><% out.print(myMem.getPhoneNumber()); %></td>
					</tr>
					<tr align="center">
						<td><a href="12_user_edit.jsp">Edit Profile</a></td>
						<td><a href="delete_user.jsp">Delete Account</a></td>
					</tr>
					<tr align="center">
						<td colspan="2"><a href="13_message_list.jsp"><input type="button" value="Messages" class="button1" /></a></td>
					</tr>
					<tr align="center">
						<td colspan="2"><a href="logout.jsp"><input type="button" value="Sign Out" class="button1" /></a></td>
					</tr>
				</table>
			</div>

			<div class="p_title">
				<h1>[Project#<%out.print(project.getId());%>] <% out.print(project.getTitle()); %></h1>
				<p align="right">
				<a href="9_project_edit.jsp?projId=<%out.print(id);%>"><input type="button" value="Edit Project" class="button1" /></a>
				<br><br><br>
				<img src="images/progressbar.png" alt="Progress Bar" width="400" height="40" id="progress_bar">
				<img src="images/mario.png" alt="Mario" width="40" height="70" id="mario">
				<span style="color:#e1e1e1; position:absolute; TOP:110px; LEFT:700px" id="percent_mario"></span>
			</div>

			<br><br>

			<div class="content">
			<table width="80%" align="left" class="pj_table">
				<tr>
					<th>Leader:</th>
					<td class="pj_leader"> <% out.print(project.getLeaderId());  %> </td>
					<th>Start:</th>
					<td class="pj_start"> <% out.print(project.getStartDate());  %> </td>
					<th>Due:</th>
					<td class="pj_due"> <% out.print(project.getDueDate());  %> </td>
				</tr>
				<tr>
					<th>Course:</th>
					<td><% out.print(project.getCourseName());  %></td>
				</tr>
				<tr>
					<th>Participants:<% //out.print(" "  + returnList1.size()); %></th>
					<td  colspan="3">
					<%
					for (int i=0; i<returnList1.size(); i++) {
						StudentBean element = (StudentBean)returnList1.get(i);
						if(i == 0)
						{out.print(element.getId());}
						else
						{out.print(", " + element.getId());}
					}
					%>
					</td>
					<form method="post" action="newParticipant.jsp?pid=<%out.print(id);%>" onsubmit="return validateForm2()">
					<td width="80"><input type="text" name="addid" id="addid"></td>
					<td><input type="submit" value="Invite" class="button1" /></td>
					</form>
				</tr>
				<tr>
					<th>Description:</th>
				</tr>
				<tr>
					<td colspan="6" align="center">
						<div class="content">
							<textarea rows="10" cols="70" readonly><% out.print(project.getDesc()); %></textarea>
						</div>
					</td>
				</tr>
			</table>		

			<div class="s_title" style="clear:both">
				<br>
				<h2>Issues</h2>
			</div>
			<table width="80%" class="list_table">
				<colgroup>
					<col width="5%" >
					<col width="40%">
					<col width="15%">
					<col width="15%">
					<col width="15%">
					<col width="10%">
				</colgroup>
				<% 
				if (returnList.size() > 0) {
				%>
				<tr>
					<th>#</th>
					<th>Issue Title</th>
					<th>In charge</th>
					<th>Created</th>
					<th>Deadline</th>
					<th>Status</th>
				</tr>
				<% 
		   		}
				else {
				%>
					<tr> No issues! </tr>
				<%
				}
				
				for (int i=0; i<returnList.size(); i++) {
					IssueBean element = (IssueBean)returnList.get(i);
				%>
				<tr align="center">
					<td><% out.print(element.getId()); %></td>
					<td><a href="4_issue_page.jsp<% out.print("?id=" + element.getId()); %> "><% out.print(element.getTitle()); %></a></td>
					<td><% out.print(element.getIdIncharge()); %></td>
					<td><% out.print(element.getCreateDate()); %></td>
					<td><% out.print(element.getDueDate()); %></td>
					<td>
					<a href="issueStatus_project_page.jsp?issueId=<%out.print(element.getId());%>&projId=<%out.print(id);%>">
					<input type="checkbox" class="switch" 
					<% if(element.getDone() == 0){
						out.print(" checked");}
					%> 
					/></a>
					</td>
				</tr>
				<% 
				}
				%>
			</table>
			<br>
			<a href="6_issue_new.jsp?id=<% out.print(id); %>"><input type="button" value="Add New Issue" class="button1" /></a>
			<br>			
			</div>
		</div>
		<%
		 	}
		 	else {
		%>
		<div class="p_title">
			<h1>You're not logged in!!</h1>
		</div>
		<br>
			<a href="11_login_page.jsp"> 로그인 페이지로 </a>
		<%
		 	}
		%>
	</body>
</html>
