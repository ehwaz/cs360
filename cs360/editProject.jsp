<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<jsp:useBean id="myMem" class="member.StudentBean" />
<jsp:useBean id="myDB" class="member.StudentManager" />

<jsp:useBean id="projObj" class="project.ProjectBean" />
<jsp:useBean id="projMgrObj" class="project.ProjectManager" />

<% 
session = request.getSession(true);
String userid=(String)session.getValue("id");

if (userid == null) {
%>
	<script>
   		window.alert('You are not logged in!!');
   		history.go(-1);
	</script>
<%	
}
else {
	String title		= request.getParameter("title");
	String desc			= request.getParameter("desc");
	String due_date		= request.getParameter("due_date");
	String start_date	= request.getParameter("start_date");
	String courseCode	= request.getParameter("course");
	String leaderName	= request.getParameter("leader");
	
	String projId		= request.getParameter("pid");
	
	// 입력값 null 여부는 .jsp쪽에서 javascript로 해준다고 가정... 입력값은 모두 valid하다!
	
	String myName = myDB.getStudent(userid).getName();
	projObj.setId(Integer.parseInt(projId));
	projObj.setTitle(title);
	projObj.setDesc(desc);
	projObj.setDueDate(due_date);
	projObj.setStartDate(start_date);
	projObj.setCourseCode(courseCode);
	projObj.setLeaderId(leaderName);
	
	try {
		projMgrObj.updateProject(projObj);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	
	response.sendRedirect("./1_project_list.jsp");
}
%>

<!-- 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<body>

</body>
</html>
 -->