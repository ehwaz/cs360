<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<jsp:useBean id="myMem" class="member.StudentBean" />
<jsp:useBean id="myDB" class="member.StudentManager" />

<jsp:useBean id="projObj" class="project.ProjectBean" />
<jsp:useBean id="projMgrObj" class="project.ProjectManager" />

<jsp:useBean id="issueBean" class="issue.IssueBean" />
<jsp:useBean id="issueManager" class="issue.IssueManager" />

<% 
session = request.getSession(true);
String userid=(String)session.getValue("id");

if (userid == null) {
%>
	<script>
   		window.alert('You are not logged in!!');
   		history.go(-1);
	</script>
<%	
}
else {
	String title		= request.getParameter("issue_title");
	String contents		= request.getParameter("issue_desc");
	String due_date		= request.getParameter("issue_deadline");
	String incharge		= request.getParameter("issue_charge");
	String projid		= request.getParameter("pid");
	
	
	issueBean.setTitle(title);
	issueBean.setContent(contents);
	issueBean.setDueDate(due_date);
	issueBean.setIdWriter(userid);
	issueBean.setIdIncharge(incharge);
	issueBean.setIdProj(Integer.parseInt(projid));

	
	try {
		issueManager.registerIssue(issueBean);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	
	response.sendRedirect("./3_project_page.jsp?id=" + projid);
}
%>