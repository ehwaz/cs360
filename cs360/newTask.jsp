<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<jsp:useBean id="issueBean" class="issue.IssueBean" />
<jsp:useBean id="issueManager" class="issue.IssueManager" />

<jsp:useBean id="taskBean" class="task.TaskBean" />
<jsp:useBean id="taskManager" class="task.TaskManager" />

<% 
session = request.getSession(true);
String userid=(String)session.getValue("id");

if (userid == null) {
%>
	<script>
   		window.alert('You are not logged in!!');
   		history.go(-1);
	</script>
<%	
}
else {
	String contents		= request.getParameter("task");
	String issueid = request.getParameter("issueId");
	
	taskBean.setContent(contents);
	taskBean.setIssueId(Integer.parseInt(issueid));

	try {
		taskManager.registerIssue(taskBean);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	
	response.sendRedirect("./4_issue_page.jsp?id=" + issueid);
}
%>