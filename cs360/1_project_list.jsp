<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ page import="java.util.ArrayList" %>
<%@ page import="project.ProjectBean" %>

 <jsp:useBean id="myMem" class="member.StudentBean" />
 <jsp:useBean id="myDB" class="member.StudentManager" />
 
<!-- <jsp:useBean id="projObj" class="project.ProjectBean" /> -->
<jsp:useBean id="projMgrObj" class="project.ProjectManager" />
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<title> Oui Cole </title>
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
	</head>
	
	<body>
		<% 
			session			= request.getSession(true);
			String userid 	= (String)session.getValue("id");
			
			ArrayList<ProjectBean> returnList = null;
		 	if( userid != null ) {
		 		myMem = myDB.getStudent(userid);
		 		returnList = projMgrObj.getProjectListByUserId(userid);	// Using plain ArrayList<Object> -_-...
		%>
		<div class="top">
			<p><img src="images/logo.PNG" alt="Oui Cole" width="160" height="50"></a></p>
		</div>
		<hr>
		
		<div class="wrapper">
			<div class="left_navi">
				<table class="member_info">
					<colgroup>
						<col width="50%">
						<col width="50%">
					</colgroup>
					<tr align="center">
						<td colspan="2" align="center"><img src="user_photo/
					<%
					out.print(myMem.getId());
					%>
					.jpg" alt="Profile" width="120" height="150" class="profile"></td>
					</tr>
					<tr align="center">
						<td colspan="2"> <% out.print(myMem.getName()); %>
 [<% out.print(myMem.getId()); %>]</td>
					</tr>
					<tr align="center">
						<td colspan="2"><% out.print(myMem.getEmail()); %></td>
					</tr>
					<tr align="center">
						<td colspan="2"><% out.print(myMem.getPhoneNumber()); %></td>
					</tr>
					<tr align="center">
						<td><a href="12_user_edit.jsp">Edit Profile</a></td>
						<td><a href="delete_user.jsp">Delete Account</a></td>
					</tr>
					<tr align="center">
						<td colspan="2"><a href="13_message_list.jsp"><input type="button" value="Messages" class="button1" /></a></td>
					</tr>
					<tr align="center">
						<td colspan="2"><a href="logout.jsp"><input type="button" value="Sign Out" class="button1" /></a></td>
					</tr>
				</table>
			</div>

			<div class="p_title">
				<h1>My projects</h1>
			</div>
			<div class="content">
			<table width="100%" class="list_table">
				<colgroup>
					<col width="5%" > 
					<col width="40%"> 
					<col width="10%"> 
					<col width="10%"> 
					<col width="10%"> 
					<col width="10%"> 
				</colgroup>
				<tr>
					<th>#</th>
					<th>Project Title</th>
					<th>Leader</th>
					<th>start date</th>
					<th>due date</th>
					<th>course</th>
					<th>#Issue</th>
					<th>#Participant</th>
				</tr>
				<% 
				for (int i=0; i<returnList.size(); i++) {
					ProjectBean element = (ProjectBean)returnList.get(i);
				%>
				<tr align="center">
					<td><% out.print(element.getId()); %></td>
					<td><a href="3_project_page.jsp<% out.print("?id=" + element.getId()); %> "><% out.print(element.getTitle()); %></a></td>
					<td><% out.print(element.getLeaderId()); %></td>
					<td><% out.print(element.getStartDate()); %></td>
					<td><% out.print(element.getDueDate()); %></td>
					<td><% out.print(element.getCourseName()); %></td>
					<td><% out.print(element.getNumberOfIssues()); %></td>
					<td><% out.print(element.getNumberOfStudents()); %></td>
				</tr>
				<% 
				}
				%>
			</table>
			<br>
			<a href="5_project_new.jsp"><input type="button" value="New Project" class="button1"/></a>
		</div>
		<%
		 	}
		 	else {
		%>
		<div class="p_title">
			<h1>You're not logged in!!</h1>
		</div>
		</div>
		<br>
			<a href="11_login_page.jsp"> 로그인 페이지로 </a>
		<%
		 	}
		%>
	 </body>
</html>
