<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<%@ page import="member.StudentBean" %>
<jsp:useBean id="myDB" class="member.StudentManager" />

<% 
session = request.getSession(true);
String userid=(String)session.getValue("id");

if (userid == null) {
%>
	<script>
   		window.alert('You are not logged in!!');
   		history.go(-1);
	</script>
<%	
}
else {
	StudentBean updateInfo = new StudentBean();
	
	String password 	= request.getParameter("password");		// 여기에서 이미 암호화되어 있어야 하는건 아닐까? 수업 플젝 구현이니 pass..
	String name			= request.getParameter("name");
	String email		= request.getParameter("email");
	String phonenumber	= request.getParameter("phonenumber");
	
	updateInfo.setId(userid);
	updateInfo.setPassword(password);
	updateInfo.setName(name);
	updateInfo.setEmail(email);
	updateInfo.setPhoneNumber(phonenumber);
	
	try {
		myDB.updateStudent(updateInfo);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	
	response.sendRedirect("./1_project_list.jsp");
}
%>