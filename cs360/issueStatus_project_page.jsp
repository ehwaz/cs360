<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	
<jsp:useBean id="issueBean" class="issue.IssueBean" />
<jsp:useBean id="issueManager" class="issue.IssueManager" />

<% 
session = request.getSession(true);
String userid=(String)session.getValue("id");

if (userid == null) {
	
%>
	<script>
   		window.alert('You are not logged in!!');
   		history.go(-1);
	</script>
<%	
}
else {
	
	String issueid = request.getParameter("issueId");
	String projid = request.getParameter("projId");
	issueBean = issueManager.getIssueByIssueId(issueid);
	
	try {
		issueManager.changeIssueStatus(issueBean);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	
	response.sendRedirect("./3_project_page.jsp?id=" + projid);
	
}

%>