<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.SQLException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>ConnectionSample</title>
</head>
<body>

	<%
	try{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = null;

		Class.forName("Altibase.jdbc.driver.AltibaseDriver");
		conn = DriverManager.getConnection("jdbc:Altibase://143.248.49.221:20300/mydb", "team3", "team3");
		query = "SELECT to_char(sysdate,'yyyy-mm-dd') FROM dual";
		stmt = conn.createStatement();
		
		//SELECT
		query = "SELECT ID, desdecrypt(PASSWORD, 'password'), name, email, phonenumber, image FROM STUDENT;";
		rs = stmt.executeQuery(query);
		while (rs.next()) {
			out.println("ID: " 			+ rs.getString(1) + 
						"<br> Password: " 	+ rs.getString(2) + 
						"<br> Name: " 		+ rs.getString(3) + 
						"<br> Email : " 		+ rs.getString(4) +
						"<br> Phone number : " + rs.getString(5) +
						"<br> Image link : " + rs.getString(6) +
						"<br>");
		}
		
		stmt.close();
		conn.close();
		rs.close();
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	%>
</body>
</html>