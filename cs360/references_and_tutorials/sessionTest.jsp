<%@ page language="java" contentType="text/html; charset=utf-8" session="true"
    pageEncoding="utf-8"%>
<%
String id = request.getParameter("id");
String password = request.getParameter("password");
String sessionId = session.getId();
long createTime = session.getCreationTime();
long lastAccessedTime = session.getLastAccessedTime();
session.setMaxInactiveInterval(60*60*15);
int interval = session.getMaxInactiveInterval();
boolean isNew = session.isNew();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
<h3>
<font color="blue">
현재 session에 대한 정보입니다.
</font>
</h3>
<br>
<%
out.println("id : " + id);
out.println("<p>");
out.println("password : " + password);
out.println("<p>");
out.println("sessionID : " + sessionId);
out.println("<p>");
out.println("createTime : " + createTime);
out.println("<p>");
out.println("lastAccessedTime : " + lastAccessedTime);
out.println("<p>");
out.println("interval : " + interval);
out.println("<p>");
out.println("isNew : " + isNew);
out.println("<p>");
%>
session 객체에 사용자의 정보를 저장하고<br>
그 정보를 이용해서 사용자를 확인합니다.&nbsp;<p>
<%
if(id.equals("cs360") && password.equals("cs360")){
session.setAttribute("id", id);
session.setAttribute("password", password);
%>
<font color="darkblue">
interjigi 님 안녕하세요!<br>
환영합니다!<br>
</font>
&nbsp;<p>
<a href="sessionTest2.jsp">
주인님 페이지로
<a>
<%
}
else{
%>
<font color="red">
주인님이 아니시군요!<br>
나가주세요!!
</font>
<%
}
%>
</body>
</html>