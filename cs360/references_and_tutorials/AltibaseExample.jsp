<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.SQLException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>Altibase JDBC Connection</title>
</head>
<body>

	<%
	try{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = null;

		Class.forName("Altibase.jdbc.driver.AltibaseDriver");
		conn = DriverManager.getConnection("jdbc:Altibase://143.248.49.221:20300/mydb", "team3", "team3");
		query = "SELECT to_char(sysdate,'yyyy-mm-dd') FROM dual";
		stmt = conn.createStatement();
		rs = stmt.executeQuery(query);

		//CREATE
		query = "CREATE TABLE actor (actorID INT, actorName VARCHAR(50), dateOfBirth VARCHAR(50), dateOfDeath VARCHAR(50), gender VARCHAR(50), numberOfAppearedMovies INT default 0, PRIMARY KEY(actorID));";
		stmt.executeUpdate(query);

		//INSERT
		query = "INSERT INTO actor (actorID, actorName, dateOfBirth, gender) VALUES ('1', 'Johnny Depp','1963.6.9','Male');";
		stmt.executeUpdate(query);
		query = "INSERT INTO actor (actorID, actorName, dateOfBirth, gender) VALUES ('2', 'Winona Ryder','1971.10.29','Female');";
		stmt.executeUpdate(query);

		//SELECT
		query = "SELECT actorID, actorName, gender FROM actor;";
		rs = stmt.executeQuery(query);
		while (rs.next()) {
			out.println("ID: " + rs.getString(1) + "Name: "
					+ rs.getString(2) + "Gender : " + rs.getString(3)
					+ "\n");
		}
		
		//DELETE
		query = "DELETE from actor where actorID='1';";
		stmt.executeUpdate(query);
		
		query = "DELETE from actor where actorID='2';";
		stmt.executeUpdate(query);

		//DROP
		query = "DROP TABLE actor;";
		stmt.executeUpdate(query);

		stmt.close();
		conn.close();
		rs.close();
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	%>
</body>
</html>