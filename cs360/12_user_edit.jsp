<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	
<jsp:useBean id="myMem" class="member.StudentBean" />
<jsp:useBean id="myDB" class="member.StudentManager" />
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<title> Oui Cole </title>
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
	</head>

	<body>
		<div class="top">
			<p><a href="1_project_list.jsp"><img src="images/logo.PNG" alt="Oui Cole" width="160" height="50"></a></p>
		</div>
		<hr>
		
		<% 
			session			= request.getSession(true);
			String userid 	= (String)session.getValue("id");
			
			if( userid != null ) {
		 		myMem = myDB.getStudent(userid);
		%>
		
		<div class="wrapper">
			<div class="left_navi">
				<table class="member_info">
					<colgroup>
						<col width="50%">
						<col width="50%">
					</colgroup>
					<tr align="center">
						<td colspan="2" align="center"><img src="user_photo/
					<%
					out.print(myMem.getId());
					%>
					.jpg" alt="Profile" width="120" height="150" class="profile"></td>
					</tr>
					<tr align="center">
						<td colspan="2"> <% out.print(myMem.getName()); %>
 [<% out.print(myMem.getId()); %>]</td>
					</tr>
					<tr align="center">
						<td colspan="2"><% out.print(myMem.getEmail()); %></td>
					</tr>
					<tr align="center">
						<td colspan="2"><% out.print(myMem.getPhoneNumber()); %></td>
					</tr>
					<tr align="center">
						<td><a href="12_user_edit.jsp">Edit Profile</a></td>
						<td><a href="delete_user.jsp">Delete Account</a></td>
					</tr>
					<tr align="center">
						<td colspan="2"><a href="logout.jsp"><input type="button" value="Sign Out" class="button1" /></a></td>
					</tr>
				</table>
			</div>
			
			<div class="p_title">
			<h1>Edit Account</h1>
			</div>
		
			<form method="post" action="editUser.jsp">
			<table class="form_table">
				<colgroup>
					<col width="30%">
					<col width="70%">
				</colgroup>
				<tr>
					<th>Password</th>
					<td><input type="password" name="password"></td>
				</tr>
				<tr>
					<th>Name</th>
					<td><input type="text" value="<% out.print(myMem.getName()); %>" name="name"></td>
				</tr>
				<tr>
					<th>Email</th>
					<td><input type="text" value="<% out.print(myMem.getEmail()); %>" name="email"></td>
				</tr>
				<tr>
					<th>Phone#</th>
					<td><input type="text" value="<% out.print(myMem.getPhoneNumber()); %>" name="phonenumber"></td>
				</tr>
			</table>
			<br>
			<input type="submit" value="Edit Account" class="button1">
			</form>
		</div>
		<%
		 	}
		 	else {
		%>
		<div class="p_title">
			<h1>You're not logged in!!</h1>
		</div>
		<br>
			<a href="11_login_page.jsp"> 로그인 페이지로 </a>
		<%
		 	}
		%>
	</body>
</html>
