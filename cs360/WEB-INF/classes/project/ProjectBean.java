package project;

import java.util.Date;
import java.util.Calendar;

public class ProjectBean {

	public ProjectBean() {
		this.id			= -1;
		this.title		= null;
		this.desc		= null;
		this.creator	= null;
		this.createDate = null;
		this.startDate	= null;
		this.dueDate	= null;
		this.lastModifiedDate = null;
		this.courseName	= null;
		this.courseCode = null;
		this.leaderId	= null;
	}
	
	// Getters
	public int getId() {
		return this.id;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public String getCreator() {
		return this.creator;
	}
	
	public String getCreateDate() {
		return this.createDate;
	}
	
	public String getStartDate() {
		return this.startDate;
	}
	
	public String getDueDate() {
		return this.dueDate;
	}
	
	public String getLastModifiedDate() {
		return this.lastModifiedDate;
	}
	
	public String getCourseName() {
		return this.courseName;
	}
	
	public String getCourseCode() {
		return this.courseCode;
	}
	
	public String getLeaderId() {
		return this.leaderId;
	}
	
	public int getNumberOfIssues() {
		return this.numberOfIssues;
	}
	
	public int getNumberOfStudents() {
		return this.numberOfStudents;
	}
	
	
	// Setters
	public void setId(int value) {
		this.id = value;
	}
	
	public void setTitle(String value) {
		this.title = value;
	}
	
	public void setDesc(String value) {
		this.desc = value;
	}
	
	public void setCreator(String value) {
		this.creator = value;
	}
	
	public void setCreateDate(String value) {
		this.createDate = value;
	}
	
	public void setStartDate(String value) {
		this.startDate = value;
	}
	
	public void setDueDate(String value) {
		this.dueDate = value;
	}
	
	public void setLastModifiedDate(String value) {
		// convert from date string to Date object
		this.lastModifiedDate = value;
	}
	
	public void setCourseName(String value) {
		this.courseName = value;
	}
	
	public void setCourseCode(String value) {
		this.courseCode = value;
	}
	
	public void setLeaderId(String value) {
		this.leaderId = value;
	}

	public void setNumberOfIssues(int value) {
		this.numberOfIssues = value;
	}
	
	public void setNumberOfStudents(int value) {
		this.numberOfStudents = value;
	}
	
	private int 	id;
	private String 	title;
    private String 	desc;
    private String 	creator;
    
    private String	createDate;
    private String	startDate;
    private String	dueDate;
    private String	lastModifiedDate;
    
    private String 	courseName;
    private String	courseCode;
    private String 	leaderId;
    
    private int numberOfIssues;
    private int numberOfStudents;
}
