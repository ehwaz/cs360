package project;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import parent.Manager;
//import member.StudentBean;

public class ProjectManager extends Manager {
	public ProjectManager() {
		super();
	}
	
	public ArrayList<ProjectBean> getProjectListByUserId(String id) {
		ArrayList<ProjectBean> 	returnList = new ArrayList<ProjectBean>();
		ResultSet 				projectSet = null;
		
		try {
			stmt = conn.createStatement();
			strQuery = "select project_id from project_participant where participant_id = '" + id + "'";
			resultSet = stmt.executeQuery(strQuery);

			while (resultSet.next()) {
				Integer tempId = resultSet.getInt("project_id");
				
				stmt = conn.createStatement();
					
				strQuery = "select p.id, p.title, p.leader_id as leader_id, to_char(p.start_date, 'YYYY-MM-DD') as s_date_string, " + 
							" to_char(p.due_date, 'YYYY-MM-DD') as d_date_string, c.course_name, " + 
							"(select count(id) from issue where project_id = '" + tempId + "') as issue_num , " + 
							"(select count(participant_id) from project_participant where project_id= '" + tempId + "') as stu_num " + 
							"from project as p,course as c where p.id = '" + tempId + "' and p.course_cd = c.course_cd ;";
				projectSet = stmt.executeQuery(strQuery);
				
				while (projectSet.next()) {
					ProjectBean tempBean = new ProjectBean();
					
					tempBean.setId(projectSet.getInt("id"));
					tempBean.setTitle(projectSet.getString("title"));
					tempBean.setLeaderId(projectSet.getString("leader_id"));
					//tempBean.setDesc(projectSet.getString("proj_desc"));
					//tempBean.setCreator(projectSet.getString("creator"));
					//tempBean.setCreateDate(projectSet.getString("create_date"));
					tempBean.setStartDate(projectSet.getString("s_date_string"));
					tempBean.setDueDate(projectSet.getString("d_date_string"));
					tempBean.setCourseName(projectSet.getString("course_name"));
					tempBean.setNumberOfIssues(projectSet.getInt("issue_num"));
					tempBean.setNumberOfStudents(projectSet.getInt("stu_num"));
					// tempBean.setLastModifiedDate(projectSet.getString("setLastModifiedDate"));
					
					returnList.add(tempBean);
				}//while
				stmt.close();
				
			} //while
  
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}

		return returnList;
	}
	
	public ProjectBean getProjectByProjectId(String id) {
		ProjectBean tempBean = new ProjectBean();;
		
		try {
			stmt = conn.createStatement();
			strQuery = "select p.id, p.title, p.leader_id as leader_id, to_char(p.start_date, 'YYYY-MM-DD') as s_date_string, " + 
					" to_char(p.due_date, 'YYYY-MM-DD') as d_date_string, c.course_name, c.course_cd, p.proj_desc " + 
					"from project as p,course as c,student as s where p.id = '" + id + "' and p.course_cd = c.course_cd and p.leader_id = s.id;";
			resultSet = stmt.executeQuery(strQuery);
			
			while (resultSet.next()) {
				tempBean.setId(resultSet.getInt("id"));
				tempBean.setTitle(resultSet.getString("title"));
				tempBean.setLeaderId(resultSet.getString("leader_id"));
				tempBean.setDesc(resultSet.getString("proj_desc"));
				//tempBean.setCreator(projectSet.getString("creator"));
				//tempBean.setCreateDate(projectSet.getString("create_date"));
				tempBean.setStartDate(resultSet.getString("s_date_string"));
				tempBean.setDueDate(resultSet.getString("d_date_string"));
				tempBean.setCourseName(resultSet.getString("course_name"));
				tempBean.setCourseCode(resultSet.getString("course_cd"));
				// tempBean.setLastModifiedDate(projectSet.getString("setLastModifiedDate"));
			}
			
			stmt.close();
			
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return tempBean;
	}
	
	public void registerProject (ProjectBean project, String creatorName) {
		try {
			// Get project Id
			stmt = conn.createStatement();
			strQuery = "select count(rownum) as project_id from project";
			resultSet = stmt.executeQuery(strQuery);
			int projectId = -1;
			while (resultSet.next()) {
				projectId = resultSet.getInt("project_id");
			}
			stmt.close();
			
			if (projectId != -1) {
				// Put project into project DB
				strQuery = "insert into project (id, title, proj_desc, creator, create_date, start_date, due_date, last_modified_date, course_cd, leader_id)" +
							"values(?, ?, ?, ?, " +
							"to_date(to_char(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'), " +
							"to_date(?,'YYYY-MM-DD'), to_date(?,'YYYY-MM-DD'), " +
							"to_date(to_char(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'), ?, ?);";
	
				pstmt = conn.prepareStatement(strQuery);
				pstmt.clearParameters();
				
				pstmt.setInt(1,	projectId);
				pstmt.setString(2,	project.getTitle());
				pstmt.setString(3,	project.getDesc());
				pstmt.setString(4,	creatorName);
				pstmt.setString(5,	project.getStartDate());
				pstmt.setString(6,	project.getDueDate());
				pstmt.setString(7,	project.getCourseCode());
				pstmt.setString(8,	project.getLeaderId());
				
				pstmt.executeUpdate();
				pstmt.close();
				
				// Put project-participant info for creator
				strQuery = "insert into project_participant (project_id, participant_id) " +
						"values(?, ?);";
	
				pstmt = conn.prepareStatement(strQuery);
				pstmt.clearParameters();
				pstmt.setInt(1,	projectId);
				pstmt.setString(2,	creatorName);
				
				pstmt.executeUpdate();
				pstmt.close();
				
				if (!creatorName.equals(project.getLeaderId())) {
					// Put project-participant info for leader
					strQuery = "insert into project_participant (project_id, participant_id) " +
							"values(?, ?);";
	
					pstmt = conn.prepareStatement(strQuery);
					pstmt.clearParameters();
					pstmt.setInt(1,	projectId);
					pstmt.setString(2,	project.getLeaderId());
					
					pstmt.executeUpdate();
					pstmt.close();
				}
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			try{ 
				if(conn!=null) {
					//conn.commit();
					conn.close();
				}
			} //����ϰ� close���Ѽ� ���ڹ�ȯ!
			catch(SQLException ex){
				ex.printStackTrace();
			}

		}//finally
	}
	
	public void updateProject (ProjectBean project) {
		try {
			// 필드가 모두 null이 아닌것이 보장되어야 함.
			stmt = conn.createStatement();
			strQuery = "update project set";

			if (project.getTitle() != "") {
				strQuery += " title='" + project.getTitle() +"',";
			}
			if (project.getDesc() != "") {
				strQuery += " proj_desc='" + project.getDesc() +"',";
			}
			if (project.getStartDate() != "") {
				strQuery += " start_date= TO_DATE('" + project.getStartDate() +"', 'YYYY-MM-DD'),";
			}
			if (project.getDueDate() != "") {
				strQuery += " due_date = TO_DATE('" + project.getDueDate() +"', 'YYYY-MM-DD'),";
			}
			if (project.getCourseCode() != "") {
				strQuery += " course_cd ='" + project.getCourseCode() +"',";
			}
			if (project.getLeaderId() != "") {
				strQuery += " leader_id ='" + project.getLeaderId() +"',";
			}
			strQuery = strQuery.substring(0, strQuery.length()-1 );
			strQuery += "  where id='" + project.getId() +"'";

			stmt.executeUpdate(strQuery);
			stmt.close();
		}

		catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			try{
				if(conn!=null) conn.close();
			}catch(SQLException ex){}
		}
	}
	
	public void deleteProject (String id) {
		try {
			stmt = conn.createStatement();
			strQuery = "delete from project where id = '" + id + "'";
			stmt.executeUpdate(strQuery);
			stmt.close();

		}catch (SQLException ex) {
			ex.printStackTrace();
		}finally {
			try{
				if(conn!=null) conn.close();
			}
			catch(SQLException ex){}
		}
	}
	
	public ArrayList<String> getCourseCodeList() {
		ArrayList<String> returnList = new ArrayList<String>();
		
		try {
			stmt = conn.createStatement();
			strQuery = "select course_cd from course";
			resultSet = stmt.executeQuery(strQuery);
			
			while (resultSet.next()) {
				returnList.add(resultSet.getString("course_cd"));
			}
			
			stmt.close();
		}catch (SQLException ex) {
			ex.printStackTrace();
		}finally {
			try{
				if(conn!=null) conn.close();
			}
			catch(SQLException ex){}
		}
		
		return returnList;
	}
}
