package participant;

import participant.ParticipantBean;
import member.StudentBean;

import issue.IssueBean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import parent.Manager;
import project.ProjectBean;
import member.StudentManager;

public class ParticipantManager extends Manager{

	public ParticipantManager(){
		super();
	}
	
	public ArrayList<StudentBean> getUserByProj(String id){
		
		ArrayList<StudentBean> returnList = new ArrayList<StudentBean>();
		ResultSet memberSet = null;
		
		try {
			stmt = conn.createStatement();
			strQuery = "select pp.participant_id "
					+ "from project_participant as pp where pp.project_id = '" + id + "';";
			resultSet = stmt.executeQuery(strQuery);

			while (resultSet.next()) {
				String tempId = resultSet.getString("participant_id");
				
				stmt = conn.createStatement();	
				
				strQuery = "select s.id, s.password, s.name, "
						+ "s.email, s.phonenumber, s.image, s.auth_type "
						+ "from student as s where s.id = '" + tempId + "';";
				memberSet = stmt.executeQuery(strQuery); 
				//
				while (memberSet.next()) {
					StudentBean tempBean = new StudentBean();
					
					tempBean.setId(memberSet.getString("id"));
					tempBean.setPassword(memberSet.getString("password"));
					tempBean.setName(memberSet.getString("name"));
					tempBean.setEmail(memberSet.getString("email"));
					tempBean.setPhoneNumber(memberSet.getString("phonenumber"));
					tempBean.setImageLink(memberSet.getString("image"));
					tempBean.setAuth(memberSet.getInt("auth_type"));;

					returnList.add(tempBean);
				}//while
				
			} //while
  
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}

		return returnList;
		
		
	}
	
	public ArrayList<ProjectBean> getProjByUser(String id){
		
		ArrayList<ProjectBean> returnList = new ArrayList<ProjectBean>();
		ResultSet projectSet = null;
		
		try {
			stmt = conn.createStatement();
			strQuery = "select pp.project_id "
					+ "from project_participant as pp where pp.participant_id = '" + id + "';";
			resultSet = stmt.executeQuery(strQuery);

			while (resultSet.next()) {
				Integer tempId = resultSet.getInt("project_id");
				
				stmt = conn.createStatement();	
				
				strQuery = "select p.id, p.title, p.proj_desc, p.creator, p.create_date, "
						+ "p.start_date, p.due_date, p.last_modified_date, p.course_cd, p.leader_id "
						+ "from project as p where p.id = '" + tempId + "';";
				projectSet = stmt.executeQuery(strQuery); 
				//
				while (projectSet.next()) {
					ProjectBean tempBean = new ProjectBean();
					
					tempBean.setId(projectSet.getInt("id"));
					tempBean.setTitle(projectSet.getString("title"));
					tempBean.setDesc(projectSet.getString("proj_desc"));
					tempBean.setCreator(projectSet.getString("creator"));
					tempBean.setCreateDate(projectSet.getString("create_date"));
					tempBean.setStartDate(projectSet.getString("start_date"));
					tempBean.setDueDate(projectSet.getString("due_date"));
					tempBean.setLastModifiedDate(projectSet.getString("last_modified_date"));
					tempBean.setCourseCode(projectSet.getString("course_cd"));
					tempBean.setLeaderId(projectSet.getString("leader_id"));

					returnList.add(tempBean);
				}//while
				
			} //while
  
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}

		return returnList;
		
		
	}
	
	public void registerParticipant(String student_id, String proj_id) {
		try {
			strQuery = "insert into project_participant (project_id, participant_id) values(?, ?);";
			pstmt = conn.prepareStatement(strQuery);
			pstmt.clearParameters();
			
			pstmt.setInt(1, Integer.parseInt(proj_id));
			pstmt.setString(2, student_id);
			
			pstmt.executeUpdate();
			pstmt.close();			
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			try{ 
				if(conn!=null) {
					//conn.commit();
					conn.close();
				}
			} //
			catch(SQLException ex){}

		}//finally
	}
	
	public boolean alreadyParticipate(String pid, String sid)
	{
		boolean check = false;
		
		try {
			stmt = conn.createStatement();
			strQuery = "select project_id as id from project_participant " +
						"where project_id = '" + pid + "' and participant_id = '" + sid + "';";
			resultSet = stmt.executeQuery(strQuery);
			if (resultSet.next()) {
				check = true;	
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return check;
	}
}
