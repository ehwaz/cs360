package participant;

public class ParticipantBean {
	
	private int project;
	private String participant;
	
	public ParticipantBean(){
		project = -1;
		participant = null;
	}
	
	
	//Setters
	public void setProject(int value){
		project = value; 
	}
	public void setParticipant(String value){
		participant = value;
	}
	
	//Getters
	
	public int getProject(){
		return project;
	}
	public String getParticipant(){
		return participant;
	}

}
