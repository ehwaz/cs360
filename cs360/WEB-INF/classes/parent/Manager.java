package parent;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Manager {
	public Manager() {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(JDBC_URL, USER, PASSWD);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	protected static final String JDBC_DRIVER = "Altibase.jdbc.driver.AltibaseDriver";
	protected static final String JDBC_URL = "jdbc:Altibase://143.248.49.221:20300/mydb";
	protected static final String USER = "team3";
	protected static final String PASSWD="team3";

	protected static ResultSet resultSet;
	protected static Statement stmt;
	protected static PreparedStatement pstmt;
	
	protected static Connection conn;
	protected static String strQuery = "";
}
