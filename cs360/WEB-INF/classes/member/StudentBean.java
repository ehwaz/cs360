package member;

public class StudentBean {
	
	public StudentBean () {
		this.id 		= "";
		this.password	= "";
		
	    this.name		= "";
	    this.email		= "";
	    this.phonenumber= "";
	    this.imageLink	= "./chonan.jpg";	// default profile
	    this.auth		= 0;				// default auth
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setPassword(String value) {
		this.password = value;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setEmail(String value) {
		this.email = value;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public void setPhoneNumber(String value) {
		this.phonenumber = value;
	}
	
	public String getPhoneNumber() {
		return this.phonenumber;
	}
	
	public void setImageLink(String value) {
		this.imageLink = value;
	}
	
	public String getImageLink() {
		return this.imageLink;
	}
	
	public void setAuth(int value) {
		this.auth = value;
	}
	
	public int getAuth() {
		return this.auth;
	}
	
	private String 	id;
	private String 	password;
	
    private String 	name;
    private String 	email;
    private String 	phonenumber;
    private String 	imageLink;
    private int 	auth;
}
