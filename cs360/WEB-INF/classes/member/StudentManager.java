package member;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

import parent.Manager;

public class StudentManager extends Manager {
	public StudentManager() {
		super();
	}
	
	public StudentBean getStudent(String id) {
		StudentBean tempBean = new StudentBean();

		try {
			stmt = conn.createStatement();
			strQuery = "select * from student where id = '"+ id +"'";
			resultSet = stmt.executeQuery(strQuery);

			while (resultSet.next()) {
				tempBean.setId(resultSet.getString("id"));
				tempBean.setPassword(resultSet.getString("password"));
				
				tempBean.setName(resultSet.getString("name"));
				tempBean.setEmail(resultSet.getString("email"));
				tempBean.setPhoneNumber(resultSet.getString("phonenumber"));
				tempBean.setImageLink(resultSet.getString("image"));
				tempBean.setAuth(resultSet.getInt("auth_type"));
			}//while

		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}

		return tempBean;
	}
	
	public void registerStudent (StudentBean student) {
		try {

			strQuery = "insert into student (id, password, name, email, phonenumber, image, auth_type)"
					+ "values(?, desencrypt(?, 'password'),?,?,?,?,?)";

			pstmt = conn.prepareStatement(strQuery);
			pstmt.clearParameters();
			pstmt.setString(1,student.getId());
			pstmt.setString(2,student.getPassword());	// TODO: encrypt
			pstmt.setString(3,student.getName());
			pstmt.setString(4,student.getEmail());
			pstmt.setString(5,student.getPhoneNumber());
			pstmt.setString(6,student.getImageLink());
			pstmt.setInt(7,student.getAuth());
			
			pstmt.executeUpdate();
			pstmt.close();
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			try{ 
				if(conn!=null) {
					//conn.commit();
					conn.close();
				}
			} 
			catch(SQLException ex){}

		}//finally
	}
	
	public void updateStudent (StudentBean student) {
		try {
			stmt = conn.createStatement();
			strQuery = "update student set";

			if (student.getPassword() != "") {
				strQuery += " password='" + student.getPassword() +"',";}
			if (student.getName() != "") {
				strQuery += " name ='" + student.getName() +"',";}
			if (student.getEmail() != "") {
				strQuery += " email='" + student.getEmail() +"',";}
			if (student.getPhoneNumber() != "") {
				strQuery += " phonenumber='" + student.getPhoneNumber() +"',";}
			
			strQuery = strQuery.substring(0, strQuery.length()-1 );
			strQuery += "  where id='" + student.getId() +"'";
			
			stmt.executeUpdate(strQuery);
			stmt.close();

		}

		catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			try{
				if(conn!=null) conn.close();
			}catch(SQLException ex){}
		}
	}
	
	public void deleteStudent (String id) {
		try {
			stmt = conn.createStatement();
			strQuery = "delete from student where id = '" + id + "'";
			stmt.executeUpdate(strQuery);
			stmt.close();

		}catch (SQLException ex) {
			ex.printStackTrace();
		}finally {
			try{
				if(conn!=null) conn.close();
			}
			catch(SQLException ex){}
		}
	}
	
	public String getUserPassword (String id) {
		String checkpass="";

		try {
			stmt = conn.createStatement();
			strQuery = "select desdecrypt(password, 'password') from student where id = '" + id + "'";
			resultSet = stmt.executeQuery(strQuery);

			if (resultSet.next()) {
				checkpass = resultSet.getString("desdecrypt(password, 'password')");	// TODO: decrypt
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return checkpass;
	}
	
	public boolean checkStudent(String id)
	{
		
		boolean check = false;
		try {
			stmt = conn.createStatement();
			strQuery = "select count(rownum) as counts from student where id = '" + id + "';";
			resultSet = stmt.executeQuery(strQuery);
			int exists = 0;
			if (resultSet.next()) {
				exists = resultSet.getInt("counts");	
			}
			if(exists > 0){check = true;}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return check;
	}
}
