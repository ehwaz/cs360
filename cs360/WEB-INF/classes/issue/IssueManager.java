package issue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import parent.Manager;
import project.ProjectBean;

public class IssueManager extends Manager{

	public IssueManager(){
		super();
	}
	
	
	
	public ArrayList<IssueBean> getIssueByUser(String id) {
		
		ArrayList<IssueBean> returnList = new ArrayList<IssueBean>();
		ResultSet issueSet = null;
		
		try {
			stmt = conn.createStatement();
			strQuery = "select i.id as issueid from issue as i join student as s on i.incharge_id = s.id where s.id = '" + id + "';";
			resultSet = stmt.executeQuery(strQuery);

			while (resultSet.next()) {
				Integer tempId = resultSet.getInt("issueid");
				
				stmt = conn.createStatement();
					
				
				strQuery = "select i.id, i.title, i.contents, to_char(i.create_date,'YYYY-MM-DD') as createdate," +
							"to_char(i.deadline_date,'YYYY-MM-DD') as deadlinedate, i.done, " +
							"to_char(i.last_modified_date, 'YYYY-MM-DD') as lastmodfieddate, " +
							"i.writer_id, i.incharge_id, i.project_id " +
							"from issue as i where i.id = '" + tempId + "';";
				issueSet = stmt.executeQuery(strQuery);

				while (issueSet.next()) {
					IssueBean tempBean = new IssueBean();
					
					tempBean.setId(issueSet.getInt("id"));
					tempBean.setTitle(issueSet.getString("title"));
					tempBean.setContent(issueSet.getString("contents"));
					tempBean.setCreateDate(issueSet.getString("createdate"));
					tempBean.setDueDate(issueSet.getString("deadlinedate"));
					tempBean.setLastModifiedDate(issueSet.getString("lastmodifieddate"));
					tempBean.setDone(issueSet.getInt("done"));
					tempBean.setIdWriter(issueSet.getString("writer_id"));
					tempBean.setIdIncharge(issueSet.getString("incharge_id"));
					tempBean.setIdProj(issueSet.getInt("project_id"));

					
					returnList.add(tempBean);
				}//while
				
			} //while
  
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}

		return returnList;
	}
	
	
	// 
	public ArrayList<IssueBean> getIssueByProj(String id) {
		
		ArrayList<IssueBean> returnList = new ArrayList<IssueBean>();
		ResultSet issueSet = null;
		
		try {
			stmt = conn.createStatement();
			strQuery = "select i.id as issueid from issue as i join project as p on i.project_id = p.id where p.id = '" 
						+ id + "' order by i.id;";
			resultSet = stmt.executeQuery(strQuery); // 

			while (resultSet.next()) {
				Integer tempId = resultSet.getInt("issueid");
				
				stmt = conn.createStatement();	
				
				strQuery = "select i.id, i.title, i.contents, to_char(i.create_date,'YYYY-MM-DD') as createdate, "
						+ "to_char(i.deadline_date,'YYYY-MM-DD') as deadlinedate, i.done, "
						+ "to_char(i.last_modified_date, 'YYYY-MM-DD') as lastmodifieddate, i.writer_id, i.incharge_id, "
						+ "i.project_id from issue as i where i.id = '" + tempId + "';";
				issueSet = stmt.executeQuery(strQuery); 
				//
				while (issueSet.next()) {
					IssueBean tempBean = new IssueBean();
					
					tempBean.setId(issueSet.getInt("id"));
					tempBean.setTitle(issueSet.getString("title"));
					tempBean.setContent(issueSet.getString("contents"));
					tempBean.setCreateDate(issueSet.getString("createdate"));
					tempBean.setDueDate(issueSet.getString("deadlinedate"));
					tempBean.setLastModifiedDate(issueSet.getString("lastmodifieddate"));
					tempBean.setDone(issueSet.getInt("done"));
					tempBean.setIdWriter(issueSet.getString("writer_id"));
					tempBean.setIdIncharge(issueSet.getString("incharge_id"));
					tempBean.setIdProj(issueSet.getInt("project_id"));

					
					returnList.add(tempBean);
				}//while
				
			} //while
  
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}

		return returnList;
	}
	
	public IssueBean getIssueByIssueId(String id) {
		IssueBean tempBean = new IssueBean();;
		
		try {
			stmt = conn.createStatement();
			strQuery = "select i.id, i.title, i.contents, to_char(i.create_date,'YYYY-MM-DD') as createdate, "
					+ "to_char(i.deadline_date,'YYYY-MM-DD') as deadlinedate, i.done, "
					+ "to_char(i.last_modified_date, 'YYYY-MM-DD') as lastmodifieddate, i.writer_id, i.incharge_id, "
					+ "i.project_id from issue as i where i.id = '" + id + "';";
			resultSet = stmt.executeQuery(strQuery);

			while (resultSet.next()) {
				tempBean.setId(resultSet.getInt("id"));
				tempBean.setTitle(resultSet.getString("title"));
				tempBean.setContent(resultSet.getString("contents"));
				tempBean.setCreateDate(resultSet.getString("createdate"));
				tempBean.setDueDate(resultSet.getString("deadlinedate"));
				tempBean.setLastModifiedDate(resultSet.getString("lastmodifieddate"));
				tempBean.setDone(resultSet.getInt("done"));
				tempBean.setIdWriter(resultSet.getString("writer_id"));
				tempBean.setIdIncharge(resultSet.getString("incharge_id"));
				tempBean.setIdProj(resultSet.getInt("project_id"));
			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return tempBean;
	}
	
	
	public void registerIssue(IssueBean issue) {
		try {
			// Get issue Id
			stmt = conn.createStatement();
			strQuery = "select count(rownum) as issue_id from issue";
			resultSet = stmt.executeQuery(strQuery);
			int issueId = -1;
			while (resultSet.next()) {
				issueId = resultSet.getInt("issue_id");
			}
			
			if (issueId != -1) {
				// Put project into project DB
				strQuery = "insert into issue (id, title, contents, create_date, deadline_date, last_modified_date, " +
							"done, writer_id, incharge_id, project_id) values(?, ?, ?, " +
							"to_date(to_char(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'), " +
							"to_date(?,'YYYY-MM-DD'), " +
							"to_date(to_char(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'), 0, ?, ?, ?);";
	
				pstmt = conn.prepareStatement(strQuery);
				pstmt.clearParameters();
				
				pstmt.setInt(1,	issueId);
				pstmt.setString(2,	issue.getTitle());
				pstmt.setString(3,	issue.getContent());
				pstmt.setString(4,	issue.getDueDate());
				pstmt.setString(5,	issue.getIdWriter());
				pstmt.setString(6,	issue.getIdIncharge());
				pstmt.setInt(7,	issue.getIdProj());
				
				pstmt.executeUpdate();
				pstmt.close();
				
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			try{ 
				if(conn!=null) {
					//conn.commit();
					conn.close();
				}
			} //
			catch(SQLException ex){}

		}//finally
	}
	
	public void updateIssue(IssueBean issue) {
		try {
			
			// 
			stmt = conn.createStatement();
			strQuery = "update issue set";
						
			if (issue.getTitle() != "") {
				strQuery += " title='" + issue.getTitle() +"',";
			}
			if (issue.getContent() != "") {
				strQuery += " contents='" + issue.getContent() +"',";
			}
			if (issue.getDueDate() != "") {
				strQuery += " deadline_date=TO_DATE('" + issue.getDueDate() +"', 'YYYY-MM-DD'),";
			}
			if (issue.getIdIncharge() != "") {
				strQuery += " incharge_id='" + issue.getIdIncharge() +"',";
			}
			strQuery = strQuery.substring(0, strQuery.length()-1 );	// removing comma
			strQuery += "  where id='" + issue.getId() +"';";

			stmt.executeUpdate(strQuery);
			stmt.close();
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			try{ 
				if(conn!=null) {
					//conn.commit();
					conn.close();
				}
			} //
			catch(SQLException ex){}

		}//finally
	}
	
	public void changeIssueStatus(IssueBean issue){
		
		try {
			stmt = conn.createStatement();
			if(issue.getDone() == 0){
				strQuery = "update issue set done=1 where id='" + issue.getId() + "';";
			}
			else{
				strQuery = "update issue set done=0 where id='" + issue.getId() + "';";
			}
			
			stmt.executeUpdate(strQuery);
			stmt.close();
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			try{ 
				if(conn!=null) {
					conn.close();
				}
			} //
			catch(SQLException ex){}
		}//finally
	}
	
	public int issueDone(String pid)
	{
		int issuedone = -1;
		
		try {
			// Get issue Id
			stmt = conn.createStatement();
			strQuery = "select count(rownum) as issuedone from issue where project_id = '" 
						+ pid + "' and done=1;";
			resultSet = stmt.executeQuery(strQuery);
			while (resultSet.next()) {
				issuedone = resultSet.getInt("issuedone");}
		}
			
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return issuedone;
	}
	
	public int issueTotal(String pid)
	{

		int issuetotal = -1;
		
		try {
			// Get issue Id
			stmt = conn.createStatement();
			strQuery = "select count(rownum) as total from issue where project_id = '" + pid + "';";
			resultSet = stmt.executeQuery(strQuery);
			while (resultSet.next()) {
				issuetotal = resultSet.getInt("total");
			}
		}
				
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		
		return issuetotal;
	}
}
