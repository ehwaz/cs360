package issue;

import java.util.Date;
import java.util.Calendar;

public class IssueBean {
	
	private int 	id;
	private String 	title;
    private String 	content;

    private String	createDate;
    private String	dueDate;
    private String	lastModifiedDate;
    
    private int done;
    private String id_writer;
    private String id_incharge;
    private int id_proj;
    
	
	public IssueBean() {
		this.id			= -1;
		this.title		= null;
		this.content	= null;
		
		this.createDate = null;
		this.dueDate	= null;
		this.lastModifiedDate = null;
		
		this.done = 0;
		this.id_writer = null;
		this.id_incharge = null;
		this.id_proj = -1;
	}
	
	//Getters
	
	public int getId(){
		return this.id;
	}
	public String getTitle(){
		return this.title;
	}
	public String getContent(){
		return this.content;
	}
	public String getCreateDate(){
		return this.createDate;
	}
	public String getDueDate(){
		return this.dueDate;
	}
	public String getLastModifiedDate(){
		return this.lastModifiedDate;
	}
	public int getDone(){
		return this.done;
	}
	public String getIdWriter(){
		return this.id_writer;
	}
	public String getIdIncharge(){
		return this.id_incharge;
	}
	public int getIdProj(){
		return this.id_proj;
	}
	
	//Setters
	
	public void setId(int value){
		this.id = value;
	}
	public void setTitle(String value){
		this.title = value;
	}
	public void setContent(String value){
		this.content = value;
	}
	public void setCreateDate(String value){
		this.createDate = value;
	}
	public void setDueDate(String value){
		this.dueDate = value;
	}
	public void setLastModifiedDate(String value){
		this.lastModifiedDate = value;
	}
	public void setDone(int value){
		this.done = value;
	}
	public void setIdWriter(String value){
		this.id_writer = value;
	}
	public void setIdIncharge(String value){
		this.id_incharge = value;
	}
	public void setIdProj(int value){
		this.id_proj = value;
	}

}
