package task;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import parent.Manager;
import issue.IssueBean;

public class TaskManager extends Manager{
	
	public TaskManager(){
		super();
	}
	
public ArrayList<TaskBean> getTaskByIssue(String id) {
		
		ArrayList<TaskBean> returnList = new ArrayList<TaskBean>();
		ResultSet taskSet = null;
		
		try {
			stmt = conn.createStatement();
			strQuery = "select t.id as taskid from task as t join issue as i " + 
						"on t.issue_id = i.id where i.id = '" + id + "' order by t.id;";
			resultSet = stmt.executeQuery(strQuery); // 

			while (resultSet.next()) {
				Integer tempId = resultSet.getInt("taskid");
				
				stmt = conn.createStatement();	
				
				strQuery = "select t.id, t.contents, t.done, t.issue_id "
						+ "from task as t where t.id = '" + tempId + "';";
				taskSet = stmt.executeQuery(strQuery); 
				//
				while (taskSet.next()) {
					TaskBean tempBean = new TaskBean();
					
					tempBean.setId(taskSet.getInt("id"));
					tempBean.setContent(taskSet.getString("contents"));
					tempBean.setDone(taskSet.getInt("done"));
					tempBean.setIssueId(taskSet.getInt("issue_id"));

					returnList.add(tempBean);
				}//while
				
			} //while
  
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}

		return returnList;
	}


	public TaskBean getTaskByTaskId(String id) {
		
		TaskBean tempBean = new TaskBean();
	
		try {
			stmt = conn.createStatement();
			strQuery = "select t.id, t.contents, t.done, t.issue_id from task as t where t.id = '" + id + "';";
			resultSet = stmt.executeQuery(strQuery);

			while (resultSet.next()) {
				tempBean.setId(resultSet.getInt("id"));
				tempBean.setContent(resultSet.getString("contents"));
				tempBean.setDone(resultSet.getInt("done"));
				tempBean.setIssueId(resultSet.getInt("issue_id"));}
		
			} 
		catch (SQLException ex) {
			ex.printStackTrace();}

	return tempBean;
	}
	

	public void registerIssue(TaskBean task) {
		try {
			// Get issue Id
			stmt = conn.createStatement();
			strQuery = "select count(rownum) as task_id from task";
			resultSet = stmt.executeQuery(strQuery);
			int taskId = -1;
			while (resultSet.next()) {
				taskId = resultSet.getInt("task_id");
			}
		
			if (taskId != -1) {
				// Put task into task DB
				strQuery = "insert into task (id, contents, done, issue_id) values(?, ?, 0, ?);";

				pstmt = conn.prepareStatement(strQuery);
				pstmt.clearParameters();
			
				pstmt.setInt(1,	taskId);
				pstmt.setString(2,	task.getContent());
				pstmt.setInt(3,	task.getIssueId());
			
				pstmt.executeUpdate();
				pstmt.close();
			
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			try{ 
				if(conn!=null) {
					//conn.commit();
					conn.close();
				}
			} //
			catch(SQLException ex){}
			
		}//finally
	}
	
	public void changeTaskStatus(TaskBean task){
		
		try {
				stmt = conn.createStatement();
				if(task.getDone() == 0){
					strQuery = "update task set done=1 where id='" + task.getId() + "';";
				}
				else{
					strQuery = "update task set done=0 where id='" + task.getId() + "';";
				}
			
				stmt.executeUpdate(strQuery);
				stmt.close();
		}
		catch (SQLException ex) {
				ex.printStackTrace();
			}
		finally {
			try{ 
				if(conn!=null) {
					conn.close();}
				} //
				catch(SQLException ex){}
		}//finally
	}

}
