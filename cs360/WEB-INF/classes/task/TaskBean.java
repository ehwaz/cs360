package task;

public class TaskBean {

	private int 	id;
    private String 	content;
    
    private int	done;
    private int		issue_id;
    
    public TaskBean(){
    	this.id = -1;
    	this.content = null;
    	this.done = 0;
    	this.issue_id = -1;
    }
    
    // Getters
    public int getId(){
		return this.id;
	}
	public String getContent(){
		return this.content;
	}
	public int getDone(){
		return this.done;
	}
	public int getIssueId(){
		return this.issue_id;
	}
	
	//Setters
	public void setId(int value){
		this.id = value;
	}
	public void setContent(String value){
		this.content = value;
	}
	public void setDone(int value){
		this.done = value;
	}
	public void setIssueId(int value){
		this.issue_id = value;
	}
}
