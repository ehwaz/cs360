package message;

public class MessageBean {
	private int 	id;
    private int 	source_id;
    private String 	desc;
    private String 	date;
    
    // Getters
    public int getId() {
		return this.id;
	}
	
	public String getDate() {
		return this.date;
	}
	
	public int getSourceId() {
		return this.source_id;
	}
	
	public String getDesc() {
		return this.desc;
	}

	// Setters
	public void setId(int value) {
		this.id = value;
	}
		
	public void setDate(String value) {
		this.date = value;
	}
	
	public void setSourceId(int value) {
		this.source_id = value;
	}
		
	public void setDesc(String value) {
		this.desc = value;
	}
}
