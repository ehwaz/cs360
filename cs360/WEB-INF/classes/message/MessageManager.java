package message;

import issue.IssueBean;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import parent.Manager;

public class MessageManager extends Manager {
	public MessageManager() {
		super();
	}
	
	public ArrayList<MessageBean> getMsgListByUser(String userId) {
		ArrayList<MessageBean> returnList = new ArrayList<MessageBean>();
		ResultSet messageSet = null;
		
		try {
			stmt = conn.createStatement();
			strQuery = "select m_id from usr_proj_list where usr_id = '" + userId + "';";
			resultSet = stmt.executeQuery(strQuery);

			while (resultSet.next()) {
				Integer msgId = resultSet.getInt("m_id");
				
				stmt = conn.createStatement();
				strQuery = "select proj_id, m_desc, to_char(create_date,'yyyy-mm-dd') as date " +
							"from message_proj where m_id = '" + msgId + "';";
				messageSet = stmt.executeQuery(strQuery);

				while (messageSet.next()) {
					MessageBean tempBean = new MessageBean();
					
					tempBean.setId(msgId);
					tempBean.setSourceId(messageSet.getInt("proj_id"));
					tempBean.setDesc(messageSet.getString("m_desc"));
					tempBean.setDate(messageSet.getString("date"));
					
					returnList.add(tempBean);
				}//while
				
			} //while
  
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return returnList;
	}
}
