package comment;

import java.util.ArrayList;
import java.sql.SQLException;

import parent.Manager;
import comment.CommentBean;

public class CommentManager extends Manager{
	public CommentManager() {
		super();
	}
	
	public ArrayList<CommentBean> getCommentListByIssueId(String issueId) {
		ArrayList<CommentBean> 	returnList = new ArrayList<CommentBean>();
		
		try {
			stmt = conn.createStatement();
			strQuery = "select id, contents, to_char(write_date, 'YYYY-MM-DD') as w_date_string, writer_id, issue_id " +
						"from comments where issue_id = '" + issueId + "'";
			resultSet = stmt.executeQuery(strQuery);
			
			while (resultSet.next()) {
				CommentBean tempBean = new CommentBean();
				
				tempBean.setId(resultSet.getInt("id"));
				tempBean.setContent(resultSet.getString("contents"));
				tempBean.setWriteDate(resultSet.getString("w_date_string"));
				tempBean.setWriterId(resultSet.getString("writer_id"));
				tempBean.setIssueId(resultSet.getInt("issue_id"));
				
				returnList.add(tempBean);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return returnList;
	}
	
	public void addComment(String comment, String writerId, int issueId) {
		try {

			strQuery = "insert into comments (id, contents, write_date, writer_id, issue_id) "
					+ "values((select count(rownum) as comment_id from comments), ?, to_date(to_char(SYSDATE,'YYYY-MM-DD'), 'YYYY-MM-DD'), ?, ?)";

			pstmt = conn.prepareStatement(strQuery);
			pstmt.clearParameters();
			pstmt.setString(1, comment);
			pstmt.setString(2, writerId);
			pstmt.setInt(3, issueId);
			
			pstmt.executeUpdate();
			pstmt.close();
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			try{ 
				if(conn!=null) {
					//conn.commit();
					conn.close();
				}
			} //����ϰ� close���Ѽ� ���ڹ�ȯ!
			catch(SQLException ex){}

		}//finally
	}
}
