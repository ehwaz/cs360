package comment;

public class CommentBean {
	public CommentBean() {
		this.id 		= -1;
		this.content 	= null;
		this.writeDate	= null;
		this.writer_id 	= null;
		this.issue_id	= -1;
	}
	
	public void setId(int value) {
		this.id = value;
	}
	
	public void setContent(String value) {
		this.content = value;
	}
	
	public void setWriteDate(String value) {
		this.writeDate = value;
	}
	
	public void setWriterId(String value) {
		this.writer_id = value;
	}
	
	public void setIssueId(int value) {
		this.issue_id = value;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getContent() {
		return this.content;
	}
	
	public String getWriteDate() {
		return this.writeDate;
	}
	
	public String getWriterId() {
		return this.writer_id;
	}
	
	public int getIssueId() {
		return this.issue_id;
	}
	
	private int 	id;
    private String 	content;
    
    private String	writeDate;
    private String	writer_id;
    private int	issue_id;
}
