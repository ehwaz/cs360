<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	
<jsp:useBean id="taskBean" class="task.TaskBean" />
<jsp:useBean id="taskManager" class="task.TaskManager" />

<jsp:useBean id="issueBean" class="issue.IssueBean" />
<jsp:useBean id="issueManager" class="issue.IssueManager" />

<% 
session = request.getSession(true);
String userid=(String)session.getValue("id");

if (userid == null) {
	
%>
	<script>
   		window.alert('You are not logged in!!');
   		history.go(-1);
	</script>
<%	
}
else {
	
	String issueid = request.getParameter("issueId");
	String taskid = request.getParameter("taskId");
	taskBean = taskManager.getTaskByTaskId(taskid);
	
	try {
		taskManager.changeTaskStatus(taskBean);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	
	response.sendRedirect("./4_issue_page.jsp?id=" + issueid);
	
}

%>