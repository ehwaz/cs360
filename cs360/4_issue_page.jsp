<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="issue.IssueBean" %>
<%@ page import="project.ProjectBean" %>

<jsp:useBean id="issuemng" class="issue.IssueManager" />
<jsp:useBean id="projMgrObj" class="project.ProjectManager" />

<jsp:useBean id="myMem" class="member.StudentBean" />
<jsp:useBean id="myDB" class="member.StudentManager" />

<%@ page import="java.util.ArrayList" %>

<jsp:useBean id="commentMgrObj" class="comment.CommentManager" />
<%@ page import="comment.CommentBean" %>

<%@ page import="task.TaskBean" %>
<jsp:useBean id="taskManager" class="task.TaskManager" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<title> Oui Cole </title>
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
		<script type="text/JavaScript" src="js/4_issue_page.js"></script>
	</head>

	<body>
		<div class="top">
			<p><a href="1_project_list.jsp"><img src="images/logo.PNG" alt="Oui Cole" width="160" height="50"></a></p>
		</div>
		<hr>
		<% 
			String id = request.getParameter("id"); 	// Issue ID
			session=request.getSession(true);
			String userid=(String)session.getValue("id"); // User ID
			
			ArrayList<CommentBean> returnList = null;
			ArrayList<TaskBean> returnList1 = null;
			
		   	if(userid != null ) {
		   		IssueBean issue = issuemng.getIssueByIssueId(id);
				String projId = Integer.toString(issue.getIdProj());
				ProjectBean project = projMgrObj.getProjectByProjectId(projId);

		   		myMem = myDB.getStudent(userid);
		   		
		   		returnList = commentMgrObj.getCommentListByIssueId(id);
		   		returnList1 = taskManager.getTaskByIssue(id);
		%>
		
		<div class="wrapper">
			<div class="left_navi">
				<table class="member_info">
					<colgroup>
						<col width="50%">
						<col width="50%">
					</colgroup>
					<tr align="center">
						<td colspan="2" align="center"><img src="user_photo/
					<%
					out.print(myMem.getId());
					%>
					.jpg" alt="Profile" width="120" height="150" class="profile"></td>
					</tr>
					<tr align="center">
						<td colspan="2"> <% out.print(myMem.getName()); %>
 [<% out.print(myMem.getId()); %>]</td>
					</tr>
					<tr align="center">
						<td colspan="2"><% out.print(myMem.getEmail()); %></td>
					</tr>
					<tr align="center">
						<td colspan="2"><% out.print(myMem.getPhoneNumber()); %></td>
					</tr>
					<tr align="center">
						<td><a href="12_user_edit.jsp">Edit Profile</a></td>
						<td><a href="delete_user.jsp">Delete Account</a></td>
					</tr>
					<tr align="center">
						<td colspan="2"><a href="13_message_list.jsp"><input type="button" value="Messages" class="button1" /></a></td>
					</tr>
					<tr align="center">
						<td colspan="2"><a href="logout.jsp"><input type="button" value="Sign Out" class="button1" /></a></td>
					</tr>
				</table>
			</div>

			<div class="p_title">
				<h2><a href="3_project_page.jsp<% out.print("?id=" + project.getId()); %>">[Project# <%out.print(project.getId());%>] <%out.print(project.getTitle());%></a></h2>
				<h1> > Issue: <%out.print(issue.getTitle());%></h1>
				<p align="right">
					<% 
					if(issue.getDone() == 0) {
					%>
					<a href="8_issue_edit.jsp?projId=<%out.print(projId);%>&issueId=<%out.print(id);%>">
					<input type="button" value="Edit Issue" class="button1"/></a>
					<a href="issueStatus.jsp?issueId=<%out.print(id);%>">
					<input type="button" value="Close Issue" class="button3" /></a>
					<%
					}
					else {%>
					<a href="issueStatus.jsp?issueId=<%out.print(id);%>">
					<input type="button" value="Reopen Issue" class="button4" /></a>
					<%
					}
					%>
				</p>
			</div>

			<div class="content">
			<table width="80%" align="left" class="pj_table">
				<tr>
					<th>In charge:</th>
					<td class="charge"><%out.print(issue.getIdIncharge());%></td>
					<th>Created:</th>
					<td class="created"><%out.print(issue.getCreateDate());%></td>
					<th>Deadline:</th>
					<td class="deadline"><%out.print(issue.getDueDate());%></td>
					<th>Status:</th>
					<td class="Status">
					<input type="button" <% if(issue.getDone() == 0) { out.print("value=\"OPEN\" class=\"label_open\""); } 
																			else { out.print("value=\"CLOSED\" class=\"label_close\""); } %> /></td>
				</tr>
				<tr>
					<th>Description:</th>
				</tr>
				<tr>
					<td colspan="6" align="center">
						<div class="content">
							<textarea rows="10" cols="70" readonly>
<%out.print(issue.getContent());%>
							</textarea>
						</div>
					</td>
				</tr>
			</table>		
			
			<div class="s_title" style="clear:both">
				<br>
				<h2>Tasks</h2>
			</div>

			<table width="70%" class="list_table">
				<colgroup>
					<col width="5%" >
					<col>
					<col width="10%">
				</colgroup>
				<% 
				if (returnList1.size() > 0) {
				%>
				<tr>
					<th>#</th>
					<th>Description</th>
					<th>Status</th>
				</tr>
				<% 
		   		}
				else {
				%>
					<tr> No tasks! </tr>
				<%
				}
				
				for (int i=0; i<returnList1.size(); i++) {
					TaskBean element1 = (TaskBean)returnList1.get(i);
				%>
				<tr align="center">
					<td><% out.print(element1.getId()); %></td>
					<td><% out.print(element1.getContent()); %></td>
					<td>
					<a href="taskStatus.jsp?taskId=<%out.print(element1.getId());%>&issueId=<%out.print(id);%>">
					<input type="checkbox" class="switch" 
					<% if(element1.getDone() == 0){
						out.print(" checked");}
					%> 
					/></a>
					</td>
				</tr>
				<% 
				}
				%>
				<tr align="center">
					<form method="post" action="newTask.jsp?issueId=<%out.print(id);%>" onsubmit="return validateForm()2">
					<td colspan="2"><input type="text" size=100 name="task" id="task"/></td>
					<td><input type="submit" value="Add Task" class="button1" /></td>
					</form>
				</tr>
			</table>
			<br>

			<div class="comment">
			<div class="s_title" style="clear:both">
				<h2>Comments</h2>
			</div>
			<table>
				<colgroup>
					<col width="15%" >
					<col>
					<col width="10%">
				</colgroup>
				<%
				for (int i=0; i<returnList.size(); i++) {
					CommentBean element = (CommentBean)returnList.get(i);
				%>
				<tr>
					<th><% out.print(element.getWriterId()); %></th>
					<td><% out.print(element.getContent()); %></td>
					<td align="center"><% out.print(element.getWriteDate()); %></td>
				</tr>
				<%
				}
				%>
				<tr>
					<th><% out.print(myMem.getId()); %></th>
					<form method="post" action="newComment.jsp?issueId=<%out.print(id);%>&writerId=<%out.print(myMem.getId());%>" onsubmit="return validateForm2()">
					<td><input type="text" name="comment" id="comment" size="95"></td>
					<td><input type="submit" value="Add Comment" class="button2" /> </td>
					</form>
				</tr>
			</table>
			</div>
		</div>
		</div>
		<%
		 	}
		 	else {
		%>
		<div class="p_title">
			<h1>You're not logged in!!</h1>
		</div>
		<br>
			<a href="11_login_page.jsp"> 로그인 페이지로 </a>
		<%
		 	}
		%>
	</body>
</html>
