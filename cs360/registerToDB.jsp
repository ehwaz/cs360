<%@ page import="member.*, java.util.*, java.io.*"%>
<%@ page import="org.apache.commons.fileupload.*"%>

 <jsp:useBean id="myMem" class="member.StudentBean" />
 <jsp:useBean id="myDB" class="member.StudentManager" />
  
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
    
    <%
	  	// if not mulit-form, just do request.getParameter("password")
	  	// String id =request.getParameter("id");
	  	
	  	// TODO: For other dev env, put your absolute path of photo folder here..
	  	String path = "C:/Users/MaGoon/MaGoon_Folder/13_Spring/DataBase/TermProject/CS360_term_project/cs360/user_photo";
	 	// TODO: It must be changed when it is deployed!!!!! Important!!
	  	// String path = "/var/lib/tomcat7/webapps/cs360/user_photo/";		// for ubuntu
	  	
	  	DiskFileUpload upload = new DiskFileUpload();
	  	upload.setSizeMax(1024 * 1024); 
	  	upload.setSizeThreshold(4096);
	  	upload.setRepositoryPath(path + "temp");
	  	
	  	List items = upload.parseRequest(request);
		Iterator iter = items.iterator();
  		String id = null;
  		
		while (iter.hasNext()) {
  	  		FileItem fileItem = (FileItem) iter.next();
  	 	 	if (fileItem.isFormField()) {
  	  			String fieldName = fileItem.getFieldName();
  	  			if (fieldName.equals("id")) {
  	  				myMem.setId(fileItem.getString());
  	  				id = fileItem.getString();
  	  			}
  	  			else if (fieldName.equals("password")) {
  	  				myMem.setPassword(fileItem.getString());
  	  			}
	  	  		else if (fieldName.equals("name")) {
	  	  			myMem.setName(fileItem.getString());
		  		}
	  	  		else if (fieldName.equals("email")) {
	  	  			myMem.setEmail(fileItem.getString());
				}
		  	  	else if (fieldName.equals("phonenumber")) {
				  	myMem.setPhoneNumber(fileItem.getString());
				}
  	  		}
		}
  	
		String userPassword = "";
		if (id != null) {
			userPassword = myDB.getUserPassword(id);
		}
		

  	if(!userPassword.equals("null") & !userPassword.equals("")) { 
  	%>

   	<script>
   	window.alert('This id is used by other user.');
   	history.go(-1);
   	</script>

  	<% 
  	} 
  	else {
  		// ========= File upload & file path setting
		try {
			iter = items.iterator();
			while (iter.hasNext()) {
	  	  		FileItem fileItem = (FileItem) iter.next();
	  	  	
	  	  		if (!fileItem.isFormField()) {
	  	  			String fileName = fileItem.getName();
	  	  			long fileSize = fileItem.getSize();
	  	  			
	  	  			fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
	  	  			String extension = fileName.substring(fileName.lastIndexOf("."));
	  	  			if (!extension.equals(".jpg")){
	  	  				break;	// TODO: error message? how?
	  	  			}
	  	 	 		fileName = id + ".jpg"; // ex) magoon.jpg
	  	  			
	  	 	 		String filePath = path + "/" + fileName;
	  	 	 		
	  	 	 		myMem.setImageLink(filePath);	
	  	  			File file = new File(filePath); 
	  	  			file.delete(); 					
	  	  			fileItem.write(file); 
	  	  			fileItem.delete();
	  	  		}
			}
		} catch (FileUploadException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
		// ========= File upload end	
				
  	myDB.registerStudent(myMem);
  	
    session = request.getSession(true);
    
   	session.putValue("id", id);

   	response.sendRedirect("./11_login_page.jsp");
   	} %> 
    
 <!--
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>

</body>
</html>  -->