<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ page import="java.util.ArrayList" %>

 <jsp:useBean id="myMem" class="member.StudentBean" />
 <jsp:useBean id="myDB" class="member.StudentManager" />
 
<jsp:useBean id="projMgrObj" class="project.ProjectManager" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
		<title> Oui Cole </title>
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
		<script type="text/JavaScript" src="js/5_project_new.js"></script>
	</head>

	<body>
		<% 
			session=request.getSession(true);
			String userid=(String)session.getValue("id");
			
		   	if(userid != null ) {
		   		myMem = myDB.getStudent(userid);
		   		ArrayList<String> courseIdList = projMgrObj.getCourseCodeList();
		%>
		<div class="top">
			<p><a href="1_project_list.jsp"><img src="images/logo.PNG" alt="Oui Cole" width="160" height="50"></a></p>
		</div>
		<hr>
		
		<div class="wrapper">
			<div class="left_navi">
				<table class="member_info">
					<colgroup>
						<col width="50%">
						<col width="50%">
					</colgroup>
					<tr align="center">
						<td colspan="2" align="center"><img src="user_photo/
					<%
					out.print(myMem.getId());
					%>
					.jpg" alt="Profile" width="120" height="150" class="profile"></td>
					</tr>
					<tr align="center">
						<td colspan="2"> <% out.print(myMem.getName()); %>
 [<% out.print(myMem.getId()); %>]</td>
					</tr>
					<tr align="center">
						<td colspan="2"><% out.print(myMem.getEmail()); %></td>
					</tr>
					<tr align="center">
						<td colspan="2"><% out.print(myMem.getPhoneNumber()); %></td>
					</tr>
					<tr align="center">
						<td><a href="12_user_edit.jsp">Edit Profile</a></td>
						<td><a href="delete_user.jsp">Delete Account</a></td>
					</tr>
					<tr align="center">
						<td colspan="2"><a href="13_message_list.jsp"><input type="button" value="Messages" class="button1" /></a></td>
					</tr>
					<tr align="center">
						<td colspan="2"><a href="logout.jsp"><input type="button" value="Sign Out" class="button1" /></a></td>
					</tr>
				</table>
			</div>

			<div class="p_title">
				<h1>Create New Project</h1>
			</div>

			<div class="content">
			<div class="error"></div>
			<form method="post" class="input_form" action="newProject.jsp" onsubmit="return validateForm2()">
			<table class="form_table">
				<colgroup>
					<col width="30%">
					<col width="70%">
				</colgroup>
				<tr>
					<th>Title</th>
					<td><input type="text" name="title" size="70" id="title"></td>
				</tr>
				<tr>
					<th>Description</th>
					<td><textarea rows="10" cols="62" name="desc" id="description"></textarea></td>
				</tr>
				<tr>
					<th>Start Date</th>
					<td><input type="text" name="start_date" class="datepick" id="start_date"></td>
				</tr>
				<tr>
					<th>Due Date</th>
					<td><input type="text"  name="due_date" class="datepick" id="due_date"></td>
				</tr>
				<tr>
					<th>Course</th>
					<td><select name="course" id="course" style="width: 155px">
					<option value="">Select course</option>
					<%
					for (int i=0; i<courseIdList.size(); i++) {
							String courseId = courseIdList.get(i);
					%>
					<option value="<% out.print(courseId); %>"><% out.print(courseId); %></option>
					<%
					}
					%>
					</select></td>
				</tr>
				<tr>
					<th>Leader ID</th>
					<td><input type="text" name="leader" id="leader"></td>
				</tr>
			</table>
			<input type="submit" value="Create Project" class="button1">
			</form>
		</div>
		</div>
		<%
		 	}
		 	else {
		%>
		<div class="p_title">
			<h1>You're not logged in!!</h1>
		</div>
		<br>
			<a href="11_login_page.jsp"> 로그인 페이지로 </a>
		<%
		 	}
		%>
	</body>
</html>
