<%@ page import="java.util.*, java.sql.*, java.io.*"%>
 <jsp:useBean id="myDB" class="member.StudentManager" />
 
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
    <%

   //session객체를 생성시키고

   session=request.getSession(true);

   //session 값이 널이 아니면 록인할때 받은값인 "id"값을 지우도록한다.

  //세션값을 줄때. setAttribute(), putValue(), getValue(),등으로 세션값을 할당으며

   //해당하는 값을 지울때는 똑같이..removeAttribute(), removeValue(), 등으로 값을 삭제하구여

   //invalidate()이메서드는 그 브라우져에 있는 모든세션값들을 지우는 메서드이죠..

  //활용도를 파악해서 잘 사용하는것이 좋을듯 ^^

   if(session != null) session.removeValue("id");

   response.sendRedirect("./11_login_page.jsp");

   %>
   
    <!-- 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>

</body>
</html>
 -->