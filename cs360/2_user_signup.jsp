<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<title> Oui Cole </title>
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
	</head>

	<body>
		<div class="top">
			<p><a href="1_project_list.jsp"><img src="images/logo.PNG" alt="Oui Cole" width="160" height="50"></a></p>
		</div>
		<hr>
		
		<div class="p_title">
			<h1>Sign Up</h1>
		</div>
		
		<div class="wrapper">
			<form method="post" action="registerToDB.jsp" enctype="multipart/form-data">
			<table class="form_table">
				<colgroup>
					<col width="30%">
					<col width="70%">
				</colgroup>
				<tr>
					<th>ID</th>
					<td><input type="text" name="id"></td>
				</tr>
				<tr>
					<th>Password</th>
					<td><input type="password" name="password"></td>
				</tr>
				<tr>
					<th>Name</th>
					<td><input type="text" name="name"></td>
				</tr>
				<tr>
					<th>Photo</th>
					<td><input type="file" name="file"></td>
				</tr>
				<tr>
					<th>Email</th>
					<td><input type="text" name="email"></td>
				</tr>
				<tr>
					<th>Phone#</th>
					<td><input type="text" name="phonenumber"></td>
				</tr>
			</table>
			<br>
			<input type="submit" value="Create Account" class="button1">
			</form>
		</div>

	</body>
</html>
