<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<title> Oui Cole </title>
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
	</head>

	<body>
		<div class ="wrapper">

			<div class="top">
			<p><img src="images/logo.PNG" alt="Oui Cole" width="160" height="50"></a></p>
			</div>
		
			<hr>
			<%
			session=request.getSession(true);
			String userid=(String)session.getValue("id");
		   	if(userid==null ) {
			%>
			<div id= "home_info">
				<div class="p_title">
					<h1>Oui Cole</h1>
				</div>
					<ul>
					  <li>Effective team project management.</li>
					  <li>Optimized for college works.</li>
					  <li>Share with friends!</li>
					</ul>
					<br>
					<a href="2_user_signup.jsp"><input type="button" value="Sign Up Now" class="button1"></a>
			</div>
		
			<div id= "login">
				<div class="p_title">
					<h1>Sign In</h1>
				</div>
				<form method="post" action="login.jsp">
					<table class="login">
						<colgroup>
							<col width="50">
							<col width="150">
						</colgroup>
						<tr>
							<th>ID:</th>
							<td><input type = "text"  name="id"/></td>
						</tr>
						<tr>
							<th>Password:</th>
							<td><input type ="password" name="password"/></td>
						</tr>
					</table>
					<br>
					<input type="submit" value="Sign In" class="button1">
				</form>
			</div>
			<%   
			} 
		   	else {
				response.sendRedirect("./1_project_list.jsp");
		   	} %>
		</div>	
	</body>
</html>
